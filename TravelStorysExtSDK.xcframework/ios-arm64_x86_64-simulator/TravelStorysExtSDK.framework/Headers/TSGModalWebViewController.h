//
//  TSGModalWebViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/22/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

#import <TravelStorysExtSDK/TSGConstants.h>
#import <TravelStorysExtSDK/TSGDeepLinkHandler.h>
#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGShorthand.h>

#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGModalWebViewController : UIViewController <TSGDeepLinkResponder, ITSGViewController, WKURLSchemeHandler, WKNavigationDelegate, WKUIDelegate>

- (void)setup;

- (void)loadURL:(NSURL*)url;

@property (assign) IBOutlet UIButton* dismissButton;
@property (assign) IBOutlet UIButton* safariButton;
@property (assign) IBOutlet UITextField* urlField;

@property (assign) IBOutlet UIView* contentView;
@property (nonatomic) WKWebView* webView;

@property (nonatomic) id localParent;

@end

NS_ASSUME_NONNULL_END

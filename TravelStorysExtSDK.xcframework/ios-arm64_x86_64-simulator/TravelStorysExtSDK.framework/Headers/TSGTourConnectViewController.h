//
//  TSGTourConnectViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import <TravelStorysExtSDK/TSGBoilerViewSupport.h>
#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDataManager.h>
#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGOrg.h>
#import <TravelStorysExtSDK/TSGShorthand.h>
#import <TravelStorysExtSDK/TSGSocialManager.h>

#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGTourConnectViewController : UIViewController <ITSGViewController, MFMessageComposeViewControllerDelegate>
{
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIButton* titleShareButton;
    
    IBOutlet UILabel* connectTitleLabel;
    IBOutlet UILabel* connectSubtitleLabel;
    
    IBOutlet UIImageView* connectImageBackground;
    IBOutlet UIImageView* connectLogo;
    
    IBOutlet UITextView* connectMessageView;
    IBOutlet UIButton* connectTargetButton;
    IBOutlet UIImageView* connectTargetIcon;
}

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

@property (nonatomic, readonly) TSGOrg* org;

- (void)loadOrg:(TSGOrg*)org fromRoute:(TSGRoute*)route;
- (void)close;

@end

NS_ASSUME_NONNULL_END

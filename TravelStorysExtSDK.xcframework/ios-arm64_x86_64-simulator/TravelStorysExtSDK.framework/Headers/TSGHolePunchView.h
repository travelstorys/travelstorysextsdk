//
//  TSGHolePunchView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/15/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class TSGHolePunchView;

@protocol TSGHolePunchViewDelegate <NSObject>

- (void)holePunchTapped:(BOOL)inside sender:(TSGHolePunchView*)sender;

@end

@interface TSGHolePunchView : UIView

@property BOOL shouldHide;
@property id<TSGHolePunchViewDelegate> delegate;

- (void)setHolePunch:(CGRect)rect;
- (void)setHolePunchWithRect:(CGRect)rect;

@end

NS_ASSUME_NONNULL_END

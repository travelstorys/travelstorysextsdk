//
//  TSGPreviewV2ViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 1/6/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGDRO.h>
#import <TravelStorysExtSDK/TSGRoute.h>

#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGConstants.h>

#import <TravelStorysExtSDK/TSGPurchaseViewController.h>

NS_ASSUME_NONNULL_BEGIN

@class TSGLanguageView;
@class TSGPopoverView;

@interface TSGPreviewV2ViewController : UIViewController <
    TSGDRODownloadDelegate,
    TSGPurchaseDelegate,
    ITSGViewController
>
{
    IBOutletCollection(UIView) NSArray* backgroundColorBoiler;
}

@property IBOutlet UIButton* closeButton;
@property IBOutlet UIView* closeThumb;

@property IBOutlet UIButton* shareButton;
@property IBOutlet UIButton* favoriteButton;

@property IBOutlet UIImageView* flagImage;
@property IBOutlet UILabel* flagLabel;

@property IBOutlet UIImageView* topImageView;
@property IBOutlet UIImageView* bottomImageView;

@property IBOutlet UIPageControl* pageControl;

@property IBOutlet UIImageView* orgImageView;
@property IBOutlet UIImageView* typeImageView;
@property IBOutlet UIImageView* type2ImageView;
@property IBOutlet UILabel* distanceLabel;
@property IBOutlet UILabel* sizeLabel;
@property IBOutlet UILabel* durLabel;

@property IBOutlet UILabel* titleLabel;
@property IBOutlet UILabel* subtitleLabel;
@property IBOutlet UILabel* orgLabel;
@property IBOutlet UILabel* summaryLabel;

@property IBOutlet TSGPopoverView* popoverView;
@property IBOutlet NSLayoutConstraint* popoverTopConstraint;

@property IBOutlet UIButton* actionButton;

@property IBOutlet UIActivityIndicatorView* activity;
@property IBOutlet UIView* swipeInterceptView;

@property (nonatomic) IBOutlet UIVisualEffectView* downloadView;
@property (nonatomic) IBOutlet UILabel* downloadTitleLabel;
@property (nonatomic) IBOutlet UILabel* downloadTypeLabel;
@property (nonatomic) IBOutlet UILabel* downloadProgressLabel;
@property (nonatomic) IBOutlet UIProgressView* downloadProgressView;
@property (nonatomic) IBOutlet UILabel* downloadSpeedLabel;

@property IBOutlet UIButton* orgIconButton;

@property IBOutlet TSGLanguageView* languageView;

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;
@property (nonatomic) TSGDRO* route;

- (void)loadRoute:(TSGDRO*)route;

@end

NS_ASSUME_NONNULL_END

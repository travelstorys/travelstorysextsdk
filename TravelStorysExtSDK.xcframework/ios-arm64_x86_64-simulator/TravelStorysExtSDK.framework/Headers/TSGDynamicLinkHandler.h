//
//  TSGDynamicLinkHandler.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/10/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <TravelStorysExtSDK/TSGLoginViewController.h>
#import <TravelStorysExtSDK/TSGPremiumUtils.h>
#import <TravelStorysExtSDK/TravelStorys.h>
#import <TravelStorysExtSDK/TSGPurchaseViewController.h>
#import <TravelStorysExtSDK/TSGPreviewViewController.h>
#import <TravelStorysExtSDK/TSGDeepLinkHandler.h>
#import <TravelStorysExtSDK/UIAlertController+Extended.h>


NS_ASSUME_NONNULL_BEGIN

@protocol TSGDynamicLinkDelegate <NSObject>

- (NSString*)createDynamicLink:(NSString*)link;
@end


@protocol TSGDynamicLinkResponder <NSObject>

@optional

- (void)loadSyncView;

@end

@interface TSGDynamicLinkHandler : NSObject 

+ (instancetype)sharedHandler;
+ (BOOL)application:(UIApplication*)application openURL:(NSURL*)url;

- (void)setAppReady;

+ (void)handleLoginOps:(NSDictionary*)opData callback:(void(^)(void))callback;

+ (void)loadRoute:(NSDictionary*)opData callback:(void(^)(void))callback;

+ (void)hotelCustomizations:(NSDictionary*)opData callback:(void(^)(void))callback;

+ (NSString*)createDynamicLink:(NSString*)link;

@property (nonatomic) id<TSGPurchaseDelegate> delegate;


@end

NS_ASSUME_NONNULL_END

//
//  TSGTourGeotagViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/6/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/ITSGViewController.h>

#import <TravelStorysExtSDK/TSGAudioManager.h>
#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDataManager.h>
#import <TravelStorysExtSDK/TSGShorthand.h>
#import <TravelStorysExtSDK/TSGSocialManager.h>

#import <TravelStorysExtSDK/TSGBoilerViewSupport.h>
#import <TravelStorysExtSDK/TSGGeotag.h>
#import <TravelStorysExtSDK/TSGMarkdownTextView.h>
#import <TravelStorysExtSDK/TSGNowPlayingView.h>
#import <TravelStorysExtSDK/TSGPagingView.h>
#import <TravelStorysExtSDK/TSGRoute.h>

#import <TravelStorysExtSDK/TSGCouponViewController.h>
#import <TravelStorysExtSDK/TSGRedeemCouponViewController.h>

#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGTourGeotagViewController : UIViewController <ITSGViewController, TSGAudioManagerSubscriber, NowPlayingViewDelegate>
{
    IBOutlet UILabel* titleLabel;
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIButton* titleVideoButton;
    IBOutlet UIButton* titleShareButton;
    IBOutlet UIButton* titleCouponButton;
    
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIImageView* firstImageView;
    IBOutlet UIImageView* secondImageView;
    
    IBOutlet TSGPagingView* pageView;
    IBOutlet TSGMarkdownTextView* bodyTextView;
    IBOutlet TSGNowPlayingView* nowPlayingView;
    
    IBOutlet UIView* captionView;
    IBOutlet UILabel* captionLabel;
    IBOutlet NSLayoutConstraint* captionBottomConstraint;
    
    IBOutlet UIView* gestureView;
    
    IBOutlet UIView* titleBar;
    IBOutlet UIImageView* playOverlay;
}

- (void)loadGeotag:(TSGGeotag*)geotag withRoute:(TSGRoute*)route shouldPlay:(BOOL)shouldPlay;

- (IBAction)toggleCaption:(id)sender;

@property (nonatomic) TSGGeotag* geotag;
@property (nonatomic) TSGRoute* route;
@property (nonatomic) TSGECoupon* coupon;

@property (nonatomic) BOOL podcast;

@property (nonatomic) BOOL usePreviewImages;
@property (nonatomic) BOOL useOrgImage;

@end

NS_ASSUME_NONNULL_END

//
//  TSGTourMapV2ViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 10/22/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGAudioPlayerView.h>
#import <TravelStorysExtSDK/TSGIndoorSelectButton.h>
#import <TravelStorysExtSDK/TSGCircleOutlineProgressView.h>

#import <TravelStorysExtSDK/TSGRoute.h>
#import <TravelStorysExtSDK/TSGTrack.h>

#import <TravelStorysExtSDK/TSGTourMapperView.h>

#import <TravelStorysExtSDK/TSGTourGeotagV2ViewController.h>
#import <TravelStorysExtSDK/TSGTourMapViewController.h>
#import <TravelStorysExtSDK/TSGBackgroundViewController.h>
#import <ifaddrs.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGTourMapV2ViewController : UIViewController <
    CLLocationManagerDelegate,
    UITableViewDelegate,
    UITableViewDataSource,

    TSGAudioManagerSubscriber,
    TSGAudioPlayerViewDelegate,
    TSGIndoorSelectButtonDelegate,
    TSGTourMapperDelegate,
    ITSGViewController,

    TSGDebugTextDelegateListener,
    TSGKontaktReceiverProtocol
>

@property IBOutlet UIView* loadingContainerView;
@property IBOutlet UILabel* loadingTitleLabel;
@property IBOutlet UILabel* loadingTrackLabel;
@property IBOutlet TSGCircleOutlineProgressView* loadingProgressView;
@property IBOutlet UIImageView* loadingBackgroundImageView;

@property IBOutlet UIButton* titleBackButton;
@property IBOutlet UIButton* titleListButton;
@property IBOutlet UIButton* titleConnectButton;
@property IBOutlet UIButton* titleMoreButton;
@property IBOutlet UILabel* titleLabel;

@property IBOutlet UIButton* connectBarButton;
@property IBOutlet UIButton* connectBarCloseButton;
@property IBOutlet UILabel* connectBarLabel;
@property IBOutlet UIImageView* connectBarIcon;

@property IBOutlet TSGTourMapperView* mapView;

@property IBOutlet UIButton* tileFunButton;
@property IBOutlet UIButton* tileAboutButton;
@property IBOutlet UIButton* tileFindMeButton;
@property IBOutlet UIButton* tileSatelliteButton;

@property IBOutlet UIView* overlayView;
@property IBOutlet UIButton* overlayButton;
@property IBOutlet UILabel* overlayLabel;
@property IBOutlet NSLayoutConstraint* overlayHeightConstraint;

@property IBOutlet TSGAudioPlayerView* audioPlayer;
@property IBOutlet TSGIndoorSelectButton* indoorSelectButton;
@property IBOutlet UIButton* indoorMinusButton;
@property IBOutlet UIButton* indoorPlusButton;

@property IBOutlet UIView* contextBackground;
@property IBOutlet UIVisualEffectView* contextContainer;
@property IBOutlet UIButton* contextCloseButton;
@property IBOutlet UITableView* contextTable;

@property IBOutlet UIView* listViewContainer;
@property IBOutlet UITableView* listViewTable;

@property IBOutlet NSLayoutConstraint* adBannerHeightConstraint;
@property IBOutlet UIView* adBannerContainer;
@property IBOutlet UIButton* adBannerButton;
@property IBOutlet UIView* adInterstitialContainer;
@property IBOutlet UIButton* adInterstitialForeground;
@property IBOutlet UIImageView* adInterstitialBackground;
@property IBOutlet UIButton* adInterstitialCloseButton;

@property IBOutlet UIView* debugContainer;
@property IBOutlet UILabel* debugZoomLabel;
@property IBOutlet UITextView* logTextView;

@property IBOutlet UIView* backgroundImageView;

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;
@property (readonly) TSGRoute* route;
@property (readonly) TSGTrack* track;

- (void)preloadRoute:(TSGRoute*)route withTrack:(TSGTrack*)track;
- (void)loadRoute;

- (BOOL)getShowingOutdoor;

- (BOOL)hasCellular;

@end

NS_ASSUME_NONNULL_END

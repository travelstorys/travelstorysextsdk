//
//  TSGBoilerViewSupport.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/28/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#ifndef BoilerViewSupport_h
#define BoilerViewSupport_h

typedef enum TSGBoilerplateValue : UInt8 {
    
    BoilerLogo,
    BoilerLogoTrans,
    
    BoilerDiscoveryBackground,
    
    BoilerLaunchLogo,
    BoilerLaunchTitle,
    BoilerLaunchSubtitle,
    BoilerLaunchDarkMode,
    
    BoilerPermissions,
    BoilerPermissionsWelcome,
    
    BoilerColorPrimary,
    BoilerColorSecondary,
    BoilerColorTertiary,
    BoilerColorNeutral,
    BoilerColorNeutral2,
    
    BoilerOptionsBackground,
    BoilerOptionsContactURL,
    BoilerOptionsPrivacyURL,
    BoilerOptionsTermsURL,
    
    BoilerAssetDownloadSmall,
    BoilerAssetDownloadMedium,
    BoilerAssetPlaySmall,
    BoilerAssetPlayMedium,
    BoilerAssetPermissionWelcome,
    BoilerAssetPermissionNotification,
    
    BoilerIconPlay,
    BoilerIconPause,
    BoilerIconCaptionArrowUp,
    BoilerIconCaptionArrowDown,
    
    BoilerIconTabNearby,
    BoilerIconTabNearbyOutline,
    BoilerIconTabMap,
    BoilerIconTabMapOutline,
    BoilerIconTabSearch,
    BoilerIconTabSearchOutline,
    BoilerIconTabFavorite,
    BoilerIconTabFavoriteOutline,
    
    BoilerTSGDeepLinkScheme
    
} TSGBoilerplateValue;

@protocol TSGBoilerViewSupport

- (id)getObjectFromBoiler:(TSGBoilerplateValue)value;

@end

typedef void (^BoilerSuccessHandler)(id<TSGBoilerViewSupport> boiler);
typedef void (^BoilerValueHandler)(id result);
typedef void (^BoilerFailedHandler)(void);

@interface TSGBoilerClass : NSObject

void GetBoilerSupport(BoilerSuccessHandler success, BoilerFailedHandler failed);
void GetBoilerValue(TSGBoilerplateValue value, BoilerValueHandler success, BoilerFailedHandler failed);

@end

#endif /* BoilerViewSupport_h */

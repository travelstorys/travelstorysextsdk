//
//  TSGIndoorSelectButton.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 1/20/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TSGIndoorSelectButtonDelegate

- (void)indexChanged:(int)index;

@end

IB_DESIGNABLE
@interface TSGIndoorSelectButton : UIButton <
    UIPickerViewDelegate,
    UIPickerViewDataSource
>

@property IBOutlet UILabel* selectedLabel;
@property (nonatomic) NSArray* options;

@property (nonatomic) int selectedIndex;
@property (nonatomic) IBInspectable NSString* selectedText;

@property (nonatomic) id<TSGIndoorSelectButtonDelegate> delegate;

@end

NS_ASSUME_NONNULL_END

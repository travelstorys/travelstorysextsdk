//
//  TSGTourGeotagV2ViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 1/21/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGAudioManager.h>
#import <TravelStorysExtSDK/TSGAudioPlayerView.h>
#import <TravelStorysExtSDK/ITSGViewController.h>

#import <TravelStorysExtSDK/TSGRoute.h>

#import <TravelStorysExtSDK/TSGMarkdownTextView.h>
#import <TravelStorysExtSDK/TSGDynamicLinkHandler.h>


NS_ASSUME_NONNULL_BEGIN

@interface TSGTourGeotagV2ViewController : UIViewController <
    TSGAudioManagerSubscriber,
    TSGAudioPlayerViewDelegate,
    ITSGViewController
>
{
    NSMutableArray<UIImage*>* cycleImages;
    BOOL cycleTopView;
    int cycleIndex;
    
    BOOL usesTimecodes;
    NSMutableArray* parsedTimecodes;
    
    BOOL captionViewOpen;
    TSGECoupon* coupon;
}

@property IBOutlet UIButton* titleBackButton;
@property IBOutlet UIButton* titleShareButton;
@property IBOutlet UIButton* titleGiftButton;
@property IBOutlet UILabel* titleLabel;

@property IBOutlet UIImageView* firstImageView;
@property IBOutlet UIImageView* secondImageView;
@property IBOutlet UIImageView* playOverlay;
@property IBOutlet UIView* gestureView;

@property IBOutlet UIPageControl* pageControl;
@property IBOutlet UIButton* fullscreenButton;

@property IBOutlet TSGMarkdownTextView* captionLabel;
@property IBOutlet UIView* captionView;
@property IBOutlet UIButton* captionToggleButton;
@property IBOutlet NSLayoutConstraint* captionHeightConstraint;

@property IBOutlet TSGMarkdownTextView* textView;
@property IBOutlet TSGAudioPlayerView* audioPlayer;

@property (readonly) TSGGeotag* geotag;
@property (readonly) TSGRoute* route;
@property (readonly) TSGTrack* currentTrack;

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

@property (nonatomic) BOOL usePreviewImages;
@property (nonatomic) BOOL useOrgImage;
@property (nonatomic) BOOL autoplayActive;

- (void)loadGeotag:(TSGGeotag*)geotag inRoute:(TSGRoute*)route shouldPlay:(BOOL)shouldPlay;

@end

NS_ASSUME_NONNULL_END

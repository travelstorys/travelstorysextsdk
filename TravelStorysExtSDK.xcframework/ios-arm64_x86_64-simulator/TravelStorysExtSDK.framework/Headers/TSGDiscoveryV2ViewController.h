//
//  TSGDiscoveryV2ViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 11/2/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGDeepLinkHandler.h>
#import <TravelStorysExtSDK/TSGDynamicLinkHandler.h>
#import <TravelStorysExtSDK/TSGDiscoveryListItemCellV2.h>
#import <TravelStorysExtSDK/ITSGViewController.h>

#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGGeotagAnnotation.h>
#import <TravelStorysExtSDK/ScoreSearch.h>

#import <TravelStorysExtSDK/TSGLoginViewController.h>
#import <TravelStorysExtSDK/TSGPasscodeViewController.h>
#import <TravelStorysExtSDK/TSGRatingViewController.h>

#import <TravelStorysExtSDK/TSGAnimBubblePointView.h>
#import <TravelStorysExtSDK/TSGHolePunchView.h>

NS_ASSUME_NONNULL_BEGIN

@class TSGNearbyManager;

@interface TSGDiscoveryV2ViewController : UIViewController <
UITableViewDataSource,
UITableViewDelegate,

CLLocationManagerDelegate,
UITextFieldDelegate,
MKMapViewDelegate,

TSGDynamicLinkResponder,
TSGDeepLinkResponder,
TSGDiscoveryListItemCellV2Delegate,
TSGLoginDelegate,
TSGSyncDelegate,
TSGPasscodeDelegate,
TSGAnimBubblePointViewDelegate,
TSGHolePunchViewDelegate,

ITSGViewController
>
{
    NSArray* sortedDros;
    UInt8 activeTab;
    
    SSCore* scoring;
}

@property (nonatomic) IBOutlet UITableView* tableView;
@property (nonatomic) IBOutlet MKMapView* mapView;
@property (nonatomic) IBOutlet UIView* mapContainer;
@property (nonatomic) IBOutlet UIView* tableContainer;

@property (nonatomic) IBOutlet UIButton* tabNearby;
@property (nonatomic) IBOutlet UIButton* tabMap;
@property (nonatomic) IBOutlet UIButton* tabSearch;
@property (nonatomic) IBOutlet UIButton* tabFavorites;
@property (nonatomic) IBOutlet UIButton* tabDownloaded;

@property (nonatomic) IBOutlet NSLayoutConstraint* searchHeightConstraint;
@property (nonatomic) IBOutlet UITextField* searchField;
@property (nonatomic) IBOutlet UILabel* notFoundLabel;

@property (nonatomic) IBOutlet UIView* contentView;

@property (nonatomic) IBOutlet TSGAnimBubblePointView* tutorialBubble;
@property (nonatomic) IBOutlet TSGHolePunchView* holePunch;

@property IBOutlet UIButton* titleAccountButton;
@property IBOutlet UIButton* titleSettingsButton;

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

@property IBOutlet UIActivityIndicatorView* activity;
@property IBOutlet UIView* activityBackingView;

- (void)openGeotagFromDynamicLink:(int)geotagId;

@end

NS_ASSUME_NONNULL_END

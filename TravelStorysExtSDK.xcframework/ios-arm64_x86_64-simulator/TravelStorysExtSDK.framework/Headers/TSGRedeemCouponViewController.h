//
//  TSGRedeemCouponViewController.h
//  TravelStorysExtSDK
//
//  Created by Matt Ellison on 3/31/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGConstants.h>
#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGCouponViewController.h>
#import <TravelStorysExtSDK/TSGDeepLinkHandler.h>
#import <TravelStorysExtSDK/TSGShorthand.h>
#import <TravelStorysExtSDK/TSGECoupon.h>
#import <TravelStorysExtSDK/TSGRoute.h>
#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGRedeemCouponViewController : UIViewController<ITSGViewController>
{
    IBOutlet UIButton* redeemButton;
    IBOutlet UIButton* closeButton;
    IBOutlet UILabel* redeemTitle;
    IBOutlet UITextView* couponDescription;
    IBOutlet UIImageView* logoView;
}

- (void)redeemCoupon:(TSGECoupon*)coupon withRoute:(TSGRoute*)route withGeotag:(TSGGeotag*)geotag;
- (void)openCouponView;
- (void)close;

@property (nonatomic) TSGECoupon* coupon;
@property (nonatomic) TSGRoute* route;
@property (nonatomic) TSGGeotag* geotag;

@end

NS_ASSUME_NONNULL_END

//
//  TSGBeaconLogic.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 6/14/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <TravelStorysExtSDK/TSGTriggerLogic.h>
#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^BeaconCallback)(int, int, BOOL, double);

@interface TSGBeaconLogic : NSObject <
    CLLocationManagerDelegate
>

+ (instancetype)sharedLogic;

- (void)startMonitoringForUUID:(NSString*)uuid;
- (void)stopMonitoringForUUID:(NSString*)uuid;

@property (nonatomic) double accuracyAdjustment;
@property (nonatomic) BeaconCallback callback;

@end

NS_ASSUME_NONNULL_END

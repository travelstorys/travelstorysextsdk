//
//  TSGCircleActivityView.h
//  TravelStorys
//
//  Created by David Worth on 11/17/16.
//  Copyright © 2016 TravelStorysGPS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSGCircleActivityView : UIView
{
    CADisplayLink* timer;
}

@property (nonatomic) float markerWidth;
@property (nonatomic) UIColor* color;
@property (nonatomic) float lineWidth;

@property (nonatomic) float rotation;

- (void)startAnimating;
- (void)stopAnimating;

@end

//
//  TSGAudioManagerSubscriber.h
//  TravelStorys
//
//  Created by David Worth on 9/15/17.
//  Copyright © 2017 TravelStorysGPS. All rights reserved.
//

/*
 * Contains the TSGAudioManagerSubscriber protocol
 */

#ifndef AudioManagerSubscriber_h
#define AudioManagerSubscriber_h

typedef enum TSGAudioPlayMode : UInt8{
    AudioPlayModeNotPlaying,
    AudioPlayModePlayingQueue,
    AudioPlayModePlayingPush
} TSGAudioPlayMode;

@protocol TSGAudioManagerSubscriber <NSObject>

/**
 * @brief called when current audio time has been updated
 *
 * @discussion this is called once per second
 *
 * @param progress
 *  The [0, 1] progress through the audio file.
 *
 * @param currentTime
 *  The current timestamp of the audio file.
 *
 * @param duration
 *  The total duration of the audio file.
 *
 */
- (void)audioManagerTimeUpdate:(float)progress withCurrentTime:(NSTimeInterval)currentTime withDuration:(NSTimeInterval)duration;

/**
 * @brief called when the audio metadata updates.
 *
 * @discussion this is called when the current audio
 * changes.
 *
 * @param title
 *  The title of the current audio file.
 *
 * @param subtitle
 *  The subtitle of the current audio file
 *  (usually the artist).
 *
 */
- (void)audioManagerDataUpdate:(NSString*)title withSubtitle:(NSString*)subtitle;

/**
 * @brief called when the audio playing status changes.
 *
 * @discussion this is called when the audio is
 * paused, resumed, skipped, or changed.
 *
 * @param isPlaying
 *  Boolean if any audio is playing
 *
 * @param playMode
 *  The current audio play mode
 *
 */
- (void)audioManagerStatusUpdate:(BOOL)isPlaying mode:(TSGAudioPlayMode)playMode;

@end

#endif /* AudioManagerSubscriber_h */

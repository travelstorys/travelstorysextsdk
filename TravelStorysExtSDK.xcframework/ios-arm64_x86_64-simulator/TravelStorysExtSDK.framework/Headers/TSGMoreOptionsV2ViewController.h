//
//  TSGMoreOptionsV2ViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 1/27/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/ITSGViewController.h>

#import <TravelStorysExtSDK/TSGLoginViewController.h>
#import <TravelStorysExtSDK/TSGPasscodeViewController.h>
#import <TravelStorysExtSDK/TSGManageViewController.h>
#import <TravelStorysExtSDK/TSGOptionGroupTableViewCell.h>

#import "UIAlertController+Extended.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGMoreOptionsV2ViewController : UIViewController <
    UITableViewDelegate,
    UITableViewDataSource,

    TSGLoginDelegate,
    TSGSyncDelegate,
    TSGPasscodeDelegate,
    TSGOptionGroupTableViewCellDelegate,

    ITSGViewController
>

@property IBOutlet UIButton* titleBackButton;
@property IBOutlet UITableView* tableView;
@property IBOutlet UILabel* version;

@property IBOutlet UIButton* titleShareButton;

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END

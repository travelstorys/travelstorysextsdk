//
//  TSGSocialManager.h
//  TravelStorys
//
//  Created by David Worth on 9/12/17.
//  Copyright © 2017 TravelStorysGPS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Social/Social.h>

@interface TSGSocialManager : NSObject

+ (TSGSocialManager*)sharedManager;

- (void)prepareShare:(NSString *)message withURL:(NSURL *)url withSender:(UIViewController *)sender withTrigger:(UIView*)trigger;
- (void)prepareShare:(NSString*)message withURL:(NSURL*)url withImage:(UIImage*)image withSender:(UIViewController*)sender;

@end

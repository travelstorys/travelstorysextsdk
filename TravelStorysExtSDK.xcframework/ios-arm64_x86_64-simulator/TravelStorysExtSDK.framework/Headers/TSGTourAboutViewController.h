//
//  TSGTourAboutViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGBoilerViewSupport.h>
#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDataManager.h>
#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGShorthand.h>
#import <TravelStorysExtSDK/TSGSocialManager.h>

#import <TravelStorysExtSDK/TSGOrg.h>
#import <TravelStorysExtSDK/TSGOrgListItem.h>
#import <TravelStorysExtSDK/TSGOrgListView.h>
#import <TravelStorysExtSDK/TSGRoute.h>

#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGTourAboutViewController : UIViewController <ITSGViewController>
{
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIButton* titleShareButton;
    
    IBOutlet TSGOrgListView* listView;
}

@property (nonatomic, readonly) TSGRoute* route;
@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

- (void)loadRoute:(TSGRoute*)route;
- (void)close;

@end

NS_ASSUME_NONNULL_END

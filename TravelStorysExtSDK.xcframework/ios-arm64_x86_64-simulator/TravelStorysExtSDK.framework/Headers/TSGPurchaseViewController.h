//
//  TSGPurchaseViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/23/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGBoilerViewSupport.h>
#import <TravelStorysExtSDK/TSGConstants.h>
#import <TravelStorysExtSDK/TSGDeepLinkHandler.h>
#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/KMLParser.h>
#import <TravelStorysExtSDK/Reachability.h>
#import <TravelStorysExtSDK/TSGShorthand.h>
#import <TravelStorysExtSDK/TSGMKMapView+Zoom.h>

#import <TravelStorysExtSDK/TSGBundleSelectView.h>
#import <TravelStorysExtSDK/TSGCircleActivityView.h>
#import <TravelStorysExtSDK/TSGPriceLabel.h>
#import <TravelStorysExtSDK/TSGRoute.h>

#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDataManager.h>
#import <TravelStorysExtSDK/TSGIAPManager.h>

#import <TravelStorysExtSDK/TSGLoginViewController.h>

#import <TravelStorysExtSDK/TSGPremiumUtils.h>
#import <TravelStorysExtSDK/TravelStorys.h>

#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TSGPurchaseDelegate <NSObject>

- (void)purchaseComplete:(NSDictionary*)ownership;

@end

@interface TSGPurchaseViewController : UIViewController <ITSGViewController, TSGLoginDelegate, MKMapViewDelegate, TSGIAPManagerDelegate, TSGBundleSelectDelegate, UIAlertViewDelegate>
{
    IBOutlet UIButton* closeButton;
    
    IBOutlet TSGPriceLabel* priceLabel;
    
    IBOutlet UIView* checkingView;
    IBOutlet UILabel* checkingLabel;
    IBOutlet TSGCircleActivityView* checkingActivityView;
    
    IBOutlet UIView* presentedView;
    IBOutlet TSGBundleSelectView* bundleView;
    IBOutlet MKMapView* mapView;
    IBOutlet UITextView* summaryView;
    IBOutlet UIView* purchaseBackground;
    IBOutlet UILabel* purchaseFinalLabel;
    IBOutlet UIButton* purchaseButton;
    
    IBOutlet UIView* thanksView;
    IBOutlet UIButton* thanksCloseButton;
    
    IBOutlet NSLayoutConstraint* bundleHeightConstraint;
    
    NSMutableArray<KMLParser*>* kmlParsers;
    
    NSMutableArray* bundleProducts;
    
    int basePrice;
    int bundlePrice;
    BOOL buyingBundle;
    
    int lifetime;
    
    int purchasePrice;
    NSString* purchaseIDs;
    int productID;
    
    NSDictionary* savedProduct;
    NSDictionary* savedBundle;
    
    NSString* initialSummaryValue;
    
    NSDictionary* activePromoCode;
}

@property (nonatomic) TSGRoute* route;
@property (nonatomic) id<TSGPurchaseDelegate> delegate;

- (void)loadProductForDRO:(TSGDRO*)dro;
- (void)loadProductInfo:(TSGRoute*)route;

@end

NS_ASSUME_NONNULL_END

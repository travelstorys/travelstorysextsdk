//
//  SSConstants.h
//  ScoreSearch
//
//  Created by David Worth on 9/11/17.
//  Copyright © 2017 TravelStorysGPS. All rights reserved.
//

#ifndef SSConstants_h
#define SSConstants_h

typedef enum SSIndex : UInt8 {
    
    SIndexNumber,
    SIndexString,
    SIndexStringArray
    
} SSIndex;

typedef enum SSMode : UInt8 {
    
    //Number Matches
    SModeNumberExact,
    SModeNumberNearlyExact,
    SModeNumberGreater,
    SModeNumberGreaterEqual,
    SModeNumberLess,
    SModeNumberLessEqual,
    
    //Number Scales
    
    
    //String Matches
    SModeStringMatchExact,
    SModeStringMatchCaseInsensitiveExact,
    SModeStringMatchPartialMatch
    
} SSMode;

typedef enum SSMethod : UInt8 {
    
    SMethodHighest,
    SMethodAverage,
    SMethodLowest
    
} SSMethod;

typedef enum SSFuzzy : UInt8
{
    FuzzyNone = 100,
    FuzzyLow = 80,
    FuzzyHigh = 55
} SSFuzzy;

#endif /* SSConstants_h */

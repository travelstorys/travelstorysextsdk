//
//  TSGRatingViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 10/1/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/ITSGViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGRatingViewController : UIViewController <UITextViewDelegate, UITextFieldDelegate, ITSGViewController>
{
    IBOutlet UIView* containerView;
    IBOutlet UIButton* rating1;
    IBOutlet UIButton* rating2;
    IBOutlet UIButton* rating4;
    IBOutlet UIButton* rating5;
    
    IBOutlet UIButton* dismiss;
    IBOutlet UIButton* submit;
    IBOutlet UIButton* close;
    
    IBOutlet UITextView* textInput;
    IBOutlet UITextField* emailInput;
    IBOutlet UIView* controlView;
    IBOutlet NSLayoutConstraint* mainHeightConstraint;
    IBOutlet NSLayoutConstraint* centerConstraint;
}

@property (nonatomic) int rating;

@end

NS_ASSUME_NONNULL_END

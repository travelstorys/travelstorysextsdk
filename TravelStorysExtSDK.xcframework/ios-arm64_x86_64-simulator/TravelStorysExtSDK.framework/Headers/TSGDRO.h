//
//  TSGDRO.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 11/5/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <TravelStorysExtSDK/TSGConstants.h>
#import <TravelStorysExtSDK/TSGJsonify.h>

#import <MapKit/MapKit.h>

#define kDRODownloadStarted @"TSG_KDRODOWNLOADSTARTED"
#define kDRODownloadFinished @"TSG_KDRODOWNLOADFINISHED"
#define kDRODownloadFailed @"TSG_KDRODOWNLOADFAILED"

struct DROReg
{
    double b;
    double lat1;
    double lng1;
    double lat2;
    double lng2;
    double m;
};

NS_ASSUME_NONNULL_BEGIN
@class TSGDRO;
@protocol TSGDRODownloadDelegate <NSObject>

- (void)DRODownloadQueued:(TSGDRO*)sender;
- (void)DRODownloadStarted:(TSGDRO*)sender;
- (void)DRODownloadUpdate:(TSGDRO*)sender filesComplete:(NSInteger)filesComplete bytesComplete:(NSInteger)bytesComplete totalFiles:(NSInteger)totalFiles;
- (void)DRODownloadFinished:(TSGDRO*)sender success:(BOOL)success;
- (void)DRODownloadSpeedUpdate:(TSGDRO*)sender withSpeed:(unsigned long long)bytesPerSecond;

- (void)DRODownloadFailed:(TSGDRO*)sender;

@end

@interface TSGDRO : NSObject <TSGJsonable>

@property (nonatomic) int routeId;
@property (nonatomic) NSString* routeKey;
@property (nonatomic) unsigned long long modified;
@property (nonatomic) NSString* minVersion;
@property (nonatomic) NSString* title;
@property (nonatomic) NSString* subtitle;
@property (nonatomic) NSString* state;
@property (nonatomic) double lat;
@property (nonatomic) double lng;
@property (nonatomic) NSArray* popovers;

@property (nonatomic) TSGRouteType type;
@property (nonatomic) ReleaseState status;
@property (nonatomic) BOOL private;
@property (nonatomic) NSString* passcode;
@property (nonatomic) BOOL premium;
@property (nonatomic) NSString* flags;
@property (nonatomic) BOOL live;
@property (nonatomic) NSString* keywords;
@property (nonatomic) NSString* dynamicLink;

@property (nonatomic) NSString* languages;

@property (nonatomic) double distance;

@property (nonatomic) NSString* discoveryImage;

@property (atomic) BOOL downloadChecked;
@property (atomic) BOOL isDownloaded;

@property (nonatomic) struct DROReg reg;

@property (atomic, nullable) id<TSGDRODownloadDelegate> delegate;
@property (atomic, nullable) id<TSGDRODownloadDelegate> secondDelegate;

- (void)startTourDownload;
- (BOOL)tourIsDownloading;

- (void)calcDistanceWithReg:(CLLocation*)location;
- (CLLocationCoordinate2D)calcDistanceWithRegReturningLocation:(CLLocation*)location;

@end

NS_ASSUME_NONNULL_END

//
//  TSGDownloadManagerv2.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/31/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kDownloadManagerNotificationRetry @"tsg-download-v2-retry-called"

NS_ASSUME_NONNULL_BEGIN

typedef void (^TSGDownloadProgressHandler)(NSInteger bytesExpected,
                                           NSInteger bytesDownloaded,
                                           NSInteger totalFiles,
                                           NSInteger filesComplete);
typedef void (^TSGDownloadFinishedHandler)(BOOL success,
                                           NSURL* url,
                                           NSData* data,
                                           NSError* error);
typedef void (^TSGDownloadCompletionHandler)(BOOL success);
typedef void (^TSGDownloadSpeedCalculationHandler)(unsigned long long bytesPerSecond);

@interface TSGDownloadManagerv2 : NSObject

@property (nonatomic, readonly) NSMutableDictionary* taskGroups;
@property (atomic, readonly) int retryCallbackCount;

+ (instancetype)sharedManager;

- (void)downloadFiles:(NSArray<NSString*>*)files
       withIdentifier:(NSString*)identifier
  withProgressHandler:(TSGDownloadProgressHandler)progress
  withFinishedHandler:(TSGDownloadFinishedHandler)finished
withCompletionHandler:(TSGDownloadCompletionHandler)completion;

- (BOOL)downloadingWithIdentifier:(NSString*)identifier;

- (void)updateHandlersWithIdentifier:(NSString*)identifier
                 withProgressHandler:(nullable TSGDownloadProgressHandler)progress
                 withFinishedHandler:(nullable TSGDownloadFinishedHandler)finished
               withCompletionHandler:(nullable TSGDownloadCompletionHandler)completion;

- (void)updateSpeedHandlerWithIdentifier:(NSString*)identifier
                        withSpeedHandler:(nullable TSGDownloadSpeedCalculationHandler)speed;

- (void)unregisterTaskGroupWithIdentifier:(NSString*)identifier;

@end

NS_ASSUME_NONNULL_END

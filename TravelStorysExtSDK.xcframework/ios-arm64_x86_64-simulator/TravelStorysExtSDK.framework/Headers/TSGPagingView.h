//
//  TSGPagingView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/7/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

IB_DESIGNABLE
@interface TSGPagingView : UIView

@property (nonatomic) IBInspectable int pages;
@property (nonatomic) IBInspectable int currentPage;
@property (nonatomic) IBInspectable int spacing;

@end

NS_ASSUME_NONNULL_END

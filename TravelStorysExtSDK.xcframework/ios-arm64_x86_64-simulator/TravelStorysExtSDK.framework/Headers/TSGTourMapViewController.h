//
//  TSGTourMapViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/28/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/ITSGViewController.h>

#import <TravelStorysExtSDK/TSGAudioManager.h>

#import <TravelStorysExtSDK/TSGBoilerViewSupport.h>
#import <TravelStorysExtSDK/TSGCircleActivityView.h>
#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGGeotag.h>
#import <TravelStorysExtSDK/TSGNowPlayingView.h>
#import <TravelStorysExtSDK/TSGRoute.h>
#import <TravelStorysExtSDK/TSGShorthand.h>
#import <TravelStorysExtSDK/TSGTourMapperView.h>
#import <TravelStorysExtSDK/TSGTrack.h>
#import <TravelStorysExtSDK/TSGTriggerLogic.h>
#import <TravelStorysExtSDK/TSGUIColor+Hex.h>
#import <TravelStorysExtSDK/TSGDeepLinkHandler.h>

#import <TravelStorysExtSDK/TSGTourGeotagViewController.h>


#import <TravelStorysExtSDK/TSGAnalyticsManager.h>


NS_ASSUME_NONNULL_BEGIN
@class TSGTourMapViewController;

@protocol TSGKontaktReceiverProtocol <NSObject>

- (void)triggerFromKontaktManager:(TSGGeotag*)geotag;
- (void)swapViewFromKontaktManager;
- (void)swapFromKontaktManager:(int)floor;

@end

@protocol TSGKontaktManagerProtocol <NSObject>

- (void)enableKontakt;
- (void)disableKontakt;
- (void)loadRegion:(NSUUID*)uuid;
- (void)setRoute:(TSGRoute*)route;
- (void)setTrack:(TSGTrack*)track;
- (void)setActiveMapController:(nullable id<TSGKontaktReceiverProtocol>)vc;

@end

@interface TSGTourMapViewController : UIViewController <CLLocationManagerDelegate, TSGTourMapperDelegate, ITSGViewController, TSGAudioManagerSubscriber, NowPlayingViewDelegate, TSGDebugTextDelegateListener, TSGKontaktReceiverProtocol>
{
    IBOutlet UIView* loadingView;
    IBOutlet UIImageView* loadingBackgroundView;
    IBOutlet UILabel* loadingTitleLabel;
    IBOutlet TSGCircleActivityView* loadingActivityView;
    
    IBOutlet UIView* titleBar;
    IBOutlet UILabel* titleLabel;
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIButton* titleSatelliteButton;
    IBOutlet UIButton* titleConnectButton;
    IBOutlet UIView* titleConnectBackground;
    IBOutlet UIButton* titleMoreButton;
    
    IBOutlet UIButton* connectBarButton;
    IBOutlet UIButton* hideConnectBarButton;
    IBOutlet UIButton* funFactsButton;
    IBOutlet UIButton* podcastButton;
    
    IBOutlet UILabel* connectBarLabel;
    
    IBOutlet UIButton* aboutTagButton;
    
    IBOutlet TSGTourMapperView* mapView;
    
    IBOutlet TSGNowPlayingView* nowPlayingView;
    
    IBOutlet NSLayoutConstraint* adHeightConstraint;
    IBOutlet NSLayoutConstraint* indoorUITopConstraint;
    
    CLLocationManager* locationManager;
    CBCentralManager* bluetoothManager;
    
    IBOutlet UILabel* debugZoomLabel;
    IBOutlet UITextView* logTextView;
    
    IBOutlet UIView* floorControlBackground;
    IBOutlet UIStepper* floorStepper;
    IBOutlet UILabel* floorLabel;
    IBOutlet UIButton* goOutsideButton;
    
    IBOutlet UIView* kmlBanner;
    IBOutlet UILabel* kmlLabel;
    
    IBOutlet NSLayoutConstraint* kmlBannerTopConstraint;
    IBOutlet NSLayoutConstraint* kmlBannerWidthConstraint;
    
    IBOutlet UIButton* directionsButton;
    IBOutlet NSLayoutConstraint* directionsButtonRightConstraint;
    TSGGeotag *currentGeotag;
    NSString* directionsURL;
    
    NSMutableArray* podcastTags;
    int podcastIndex;
}

- (void)prepRoute:(TSGRoute*)route withTrackId:(int)trackId;

- (void)showAbout;
- (void)close;
- (void)connect;

- (void)playPodcast;

- (void)triggerFromKontaktManager:(TSGGeotag*)geotag;
- (void)swapViewFromKontaktManager;
- (void)swapFromKontaktManager:(int)floor;

- (void)handleDirectionsWithGeotag:(TSGGeotag *)geotag;
- (void)openAppleMapsWithGeotag;
- (void)hideDirectionsButton;

@property (nonatomic, readonly) TSGRoute* route;
@property (nonatomic, readonly) TSGTrack* track;
@property (nonatomic, readonly) int trackId;
@property (nonatomic, readonly) NSUUID* routeUUID;


@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END

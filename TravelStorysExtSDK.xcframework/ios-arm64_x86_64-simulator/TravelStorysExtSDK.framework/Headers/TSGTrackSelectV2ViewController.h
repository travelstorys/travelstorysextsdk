//
//  TSGTrackSelectV2ViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 1/11/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGTrackSelectListItem.h>
#import <TravelStorysExtSDK/TSGDRO.h>
#import <TravelStorysExtSDK/TSGRoute.h>

#import <TravelStorysExtSDK/ITSGViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGTrackSelectV2ViewController : UIViewController <
    UITableViewDataSource,
    UITableViewDelegate,
    ITSGViewController
>

@property IBOutlet UIButton* backButton;
@property IBOutlet UITableView* tableView;

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;
@property (readonly) TSGRoute* route;

- (void)loadRouteWithDRO:(TSGDRO*)dro;
- (void)loadRoute:(TSGRoute*)route;

@end

NS_ASSUME_NONNULL_END

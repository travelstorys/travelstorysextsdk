//
//  TSGTrackSelectViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/24/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGBoilerViewSupport.h>
#import <TravelStorysExtSDK/TSGConstants.h>
#import <TravelStorysExtSDK/TSGDeepLinkHandler.h>
#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGShorthand.h>

#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDataManager.h>

#import <TravelStorysExtSDK/TSGTrackSelectItem.h>

#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGTrackSelectViewController : UIViewController <ITSGViewController>
{
    IBOutlet UIImageView* backgroundImageView;
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIScrollView* scrollView;
}

- (void)loadRoute:(TSGERO*)route;

@property (readonly) TSGRoute* route;
@property id<ITSGViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END

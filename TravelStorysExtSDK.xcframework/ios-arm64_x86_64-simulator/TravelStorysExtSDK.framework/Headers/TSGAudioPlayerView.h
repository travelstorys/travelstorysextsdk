//
//  TSGAudioControlsView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 1/14/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum AudioPlayerViewStyle : UInt8
{
    AudioViewStyleTitleSubtitle,
    AudioViewStyleTimecode
} AudioPlayerViewStyle;

@protocol TSGAudioPlayerViewDelegate

- (void)audioPlayerViewPressedPlay;
- (void)audioPlayerViewPressedPause;
- (void)audioPlayerViewSoftUpdate:(float)progressValue;
- (void)audioPlayerViewHardUpdate:(float)progressvalue;

@end

@interface TSGAudioPlayerView : UIView

@property IBOutlet UILabel* titleLabel;
@property IBOutlet UILabel* subtitleLabel;
@property IBOutlet UILabel* timeLabel;
@property IBOutlet UIView* thumb;
@property IBOutlet UIView* track;
@property IBOutlet UIView* trackFill;
@property IBOutlet UIButton* actionButton;

@property (nonatomic) AudioPlayerViewStyle viewStyle;
@property (nonatomic) id<TSGAudioPlayerViewDelegate> delegate;

@property (nonatomic) double progress;

- (void)setTimecode:(NSTimeInterval)elapsed withDuration:(NSTimeInterval)duration;
- (void)updatePlayState:(BOOL)isPlaying;
- (void)hideAudioBar;

@end

NS_ASSUME_NONNULL_END

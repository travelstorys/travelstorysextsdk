//
//  TSGCircleProgressView.h
//
//  Created by David Worth on 9/14/16.
//  Copyright © 2016 Rahket, LLC. All rights reserved.
//
//  Used with Permission.
//

#import <UIKit/UIKit.h>

@interface TSGCircleProgressView : UIView
{
    UIImageView* arrowView;
}

@property (nonatomic, readonly) float progress;
@property (nonatomic) UIColor* color;
@property (nonatomic) float lineWidth;

@property (nonatomic) UIImageView* icon;

- (void)setProgress:(float)progress;

@end

//
//  TSGPreviewViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/13/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/ITSGViewController.h>

#import <TravelStorysExtSDK/TSGBoilerViewSupport.h>
#import <TravelStorysExtSDK/TSGCircleProgressView.h>
#import <TravelStorysExtSDK/TSGCRO.h>
#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDownloadManager.h>
#import <TravelStorysExtSDK/TSGDownloadManagerv2.h>
#import <TravelStorysExtSDK/TSGERO.h>
#import <TravelStorysExtSDK/TSGGeotag.h>
#import <TravelStorysExtSDK/TSGHolePunchView.h>
#import <TravelStorysExtSDK/TSGPriceLabel.h>
#import <TravelStorysExtSDK/TSGRoute.h>
#import <TravelStorysExtSDK/TSGSocialManager.h>
#import <TravelStorysExtSDK/TSGSplitTabView.h>
#import <TravelStorysExtSDK/TSGTourMapperView.h>

#import <TravelStorysExtSDK/TSGPurchaseViewController.h>

#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGPreviewViewController : UIViewController <TSGSplitTabViewDelegate, ITSGViewController, DownloadProgressDelegate, TSGPurchaseDelegate>
{
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIButton* titleShareButton;
    
    IBOutlet TSGSplitTabView* tabView;
    
    IBOutlet UIImageView* firstImageView;
    IBOutlet UIImageView* secondImageView;
    
    IBOutlet UIImageView* orgImageView;
    IBOutlet UILabel* routeTitleLabel;
    IBOutlet UILabel* routePresentedLabel;
    IBOutlet UIButton* orgLinkButton;
    NSString* orgSite;
    
    IBOutlet UIImageView* locationImage;
    IBOutlet UIImageView* timeImage;
    
    IBOutlet UILabel* locationLabel;
    IBOutlet UILabel* timeLabel;
    
    IBOutlet TSGPriceLabel* routePriceLabel;
    
    IBOutlet UILabel* summaryLabel;
    IBOutlet UIButton* actionButton;
    
    //TSGCircleProgressView* progressView;
    
    IBOutlet TSGTourMapperView* mapView;
    
    IBOutlet UIImageView* premiumBackground;
    IBOutlet UILabel* premiumLabel;
    
    IBOutlet UIVisualEffectView* downloadingView;
    IBOutlet UIProgressView* progressView;
    IBOutlet UILabel* downloadingTitleLabel;
    IBOutlet UILabel* downloadingSizeLabel;
    
    IBOutlet TSGHolePunchView* promptCover;
    IBOutlet UIButton* dontShowButton;
    
    IBOutlet UIView* bandwidthMessage;
    IBOutlet NSLayoutConstraint* bandwidthTopConstraint;
}

- (void)loadRoute:(TSGERO*)route;
- (void)playAction;
- (void)close;

- (IBAction)hideLowBandwidth:(id)sender;

@property (nonatomic, readonly) TSGERO* route;

@property id<ITSGViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END

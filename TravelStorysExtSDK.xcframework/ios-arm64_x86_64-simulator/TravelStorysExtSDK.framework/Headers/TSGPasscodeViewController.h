//
//  TSGPasscodeViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGBoilerViewSupport.h>
#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGPassDotsView.h>
#import <TravelStorysExtSDK/TSGShorthand.h>

#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

#import <AppTrackingTransparency/AppTrackingTransparency.h>
#import <AdSupport/AdSupport.h>

NS_ASSUME_NONNULL_BEGIN

@class TSGPasscodeViewController;

@protocol TSGPasscodeDelegate <NSObject>

- (void)passcodeEntered:(NSString*)passcode withSender:(TSGPasscodeViewController*)sender;

@end

typedef enum TSGPasscodeMode : UInt8
{
    PasscodeDefault,
    PasscodeDeveloper
} TSGPasscodeMode;

@interface TSGPasscodeViewController : UIViewController <ITSGViewController>
{
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIButton* titleBackButton;
    IBOutlet UILabel* titleLabel;
    
    IBOutlet UILabel* passcodeLabel;
    IBOutlet UILabel* passcodeDetailLabel;
    
    IBOutlet TSGPassDotsView* dots;
    IBOutlet UITextField* inputField;
    
    IBOutlet UIView* developerView;
    IBOutlet UIView* passcodeView;
    
    IBOutlet UISwitch* developerDebugMenuSlider;
    IBOutlet UISwitch* developerDebugDBSlider;
    IBOutlet UIButton* developerTutorialButton;
}

- (void)resetAsWrong;
- (void)resetAsRight;

- (void)showDeveloperMenu;

@property (nonatomic) id<TSGPasscodeDelegate> pDelegate;
@property (nonatomic) TSGPasscodeMode mode;

@end

NS_ASSUME_NONNULL_END

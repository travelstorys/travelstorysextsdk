//
//  TSGBackgroundViewController.h
//  TravelStorysExtSDK
//
//  Created by Matt Ellison on 10/11/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TravelStorysExtSDK/TSGConstants.h>
#import <TravelStorysExtSDK/ITSGViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGBackgroundViewController : UIViewController <ITSGViewController>

@end

NS_ASSUME_NONNULL_END

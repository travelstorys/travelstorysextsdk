//
//  TSGLoginViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 6/18/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TravelStorysExtSDK/TravelStorys.h>

#import <AuthenticationServices/AuthenticationServices.h>

#import <TravelStorysExtSDK/TSGConstants.h>
#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGShorthand.h>
#import <TravelStorysExtSDK/TSGMarkdownTextView.h>

#import <TravelStorysExtSDK/TSGSyncViewController.h>

#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

#import <TravelStorysExtSDK/TSGModalWebViewController.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TSGLoginDelegate <NSObject>

- (void)loginComplete:(TravelStorysLogin)status;
- (void)loginSkipped;
- (void)loginClosed;

@end

@protocol TSGLoginFacebookDelegate <NSObject>

- (void)facebookLogin:(UIViewController*)sender withCallback:(void (^)(BOOL, NSString *, NSString *))callback;

@end


@interface TSGLoginViewController : UIViewController <ITSGViewController, UITextFieldDelegate, TSGSyncDelegate, ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding>
{
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIButton* closeButton;
    
    IBOutlet UIView* promptView;
    IBOutlet UIView* loginView;
    IBOutlet UIView* registerView;
    
    IBOutlet UIButton* promptLoginButton;
    IBOutlet UIButton* promptRegisterButton;
    IBOutlet UIButton* promptFacebookButton;
    IBOutlet UIImageView* promptFacebookIcon;
    
    IBOutlet UIButton* promptSIWAButton;
    
    IBOutlet UIButton* promptSkipButton;
    IBOutlet UIImageView* promptSkipIcon;
    IBOutlet UITextView* promptTerms;
    
    IBOutlet UIButton* loginBackButton;
    IBOutlet UIButton* loginSubmitButton;
    IBOutlet UIButton* loginForgotButton;
    IBOutlet UITextField* loginEmailField;
    IBOutlet UITextField* loginPassField;
    
    IBOutlet UIButton* registerBackButton;
    IBOutlet UIButton* registerSubmitButton;
    IBOutlet UITextField* registerFirstField;
    IBOutlet UITextField* registerLastField;
    IBOutlet UITextField* registerEmailField;
    IBOutlet UITextField* registerPassField;
    IBOutlet UITextField* registerConfirmField;
    IBOutlet UITextView* registerTerms;
    
    IBOutlet NSLayoutConstraint* promptLeadingConstraint;
    IBOutlet NSLayoutConstraint* loginLeadingConstraint;
    IBOutlet NSLayoutConstraint* registerLeadingConstraint;
    
    IBOutlet UILabel* titleLabel;
    
}

@property (nonatomic) id<TSGLoginDelegate> delegate;



@end

NS_ASSUME_NONNULL_END

//
//  TSGSessionManager.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/20/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

/**
 *
 */

#import <Foundation/Foundation.h>

#import <TravelStorysExtSDK/TravelStorys.h>

#import <TravelStorysExtSDK/TSGBackgroundManager.h>
#import <TravelStorysExtSDK/TSGDownloadManager.h>
#import <TravelStorysExtSDK/TSGDownloadManagerv2.h>

#import <TravelStorysExtSDK/TSGDatabase.h>

#import <TravelStorysExtSDK/Reachability.h>
#import <TravelStorysExtSDK/TSGShorthand.h>

typedef enum TSGAppNetworkMode : UInt8
{
    AppNetworkOnline,
    AppNetworkOffline
} TSGAppNetworkMode;

NS_ASSUME_NONNULL_BEGIN

typedef enum TSGSessionCallbackValue : UInt8
{
    SessionUpdateStarted,
    SessionUpdateFinished,
    
    SessionNetworkCheckStarted,
    SessionNetworkCheckPassed,
    SessionNetworkCheckFailedNoNetwork,
    SessionNetworkCheckFailedBadNetwork,
    
    SessionUpdateCheckStarted,
    SessionUpdateCheckPassed,
    SessionUpdateCheckPassedOptional,
    SessionUpdateCheckFailedRequired,
    
    SessionConnectAPIStarted,
    SessionConnectAPIComplete,
    SessionConnectAPIFailed,
    
    SessionAPIUpdateStarted,
    SessionAPIUpdateComplete,
    SessionAPIUpdateFailed,
    
    SessionAPIUpdateRouteProgress,
    
    SessionFileCheckStart,
    SessionFileCheckUpdate,
    SessionFileCheckComplete,
    
    SessionFileDownloadStarted,
    SessionFileDownloadUpdate,
    SessionVerifyAccountUpdate,
    SessionVerifyAccountComplete,
    SessionFileDownloadComplete,
    SessionFileDownloadFailed
    
} TSGSessionCallbackValue;

@protocol TSGSessionManagerDelegate <NSObject>

- (void)sessionUpdateCallback:(TSGSessionCallbackValue)value withObject:(__nullable id)object;

@end

@interface TSGSessionManager : NSObject <DownloadProgressDelegate>

+ (TSGSessionManager*)shared;

- (void)updateSession;
- (void)proceedWithoutOptionalUpdate;

@property (nonatomic) TSGAppNetworkMode networkMode;
@property (nonatomic) id<TSGSessionManagerDelegate> delegate;

@property (atomic) int updateFinished;

@end

NS_ASSUME_NONNULL_END

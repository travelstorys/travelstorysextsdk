//
//  TSGOrgListItem.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDataManager.h>
#import <TravelStorysExtSDK/TSGOrg.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGOrgListItem : UIView
{
    IBOutlet UIImageView* logoImageView;
    IBOutlet UILabel* orgLabel;
    IBOutlet UIButton* webButton;
}

- (void)loadOrg:(TSGOrg*)org withStripe:(BOOL)stripe;

+ (TSGOrgListItem*)loadFromNib;

@end

NS_ASSUME_NONNULL_END

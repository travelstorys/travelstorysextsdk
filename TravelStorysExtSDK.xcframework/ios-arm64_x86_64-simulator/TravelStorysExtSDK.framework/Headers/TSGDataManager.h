//
//  TSGDataManager.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/26/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGNSString+Hash.h>
#import <TravelStorysExtSDK/Reachability.h>
#import <TravelStorysExtSDK/TSGShorthand.h>

#import <TravelStorysExtSDK/TSGDRO.h>
#import <TravelStorysExtSDK/TSGRoute.h>

NS_ASSUME_NONNULL_BEGIN

#define DataDirectoryConfig     @"DataConfig"
#define DataDirectoryRequired   @"DataRequired"
#define DataDirectoryRoute      @"DataRoute"
#define DataDirectoryTemp       @"DataTemp"

@interface TSGDataManager : NSObject

+ (NSString*)pathForDirectory:(NSString*)directory;
+ (NSString*)pathForDirectory:(NSString *)directory withName:(NSString*)name;
+ (NSString*)pathForDirectory:(NSString *)directory withName:(nullable NSString*)name withURL:(NSString *)url;

+ (BOOL)dataIsCachedWithDirectory:(NSString*)directory withName:(nullable NSString*)name withURL:(NSString*)url;
+ (void)saveData:(NSData *)data withDirectory:(NSString *)directory withName:(nullable NSString *)name withURL:(NSString *)url;
+ (NSData*)loadDataAtPath:(NSString*)path;
+ (NSData*)loadDataWithDirectory:(NSString*)directory withName:(nullable NSString*)name withURL:(NSString*)url;

+ (BOOL)checkHeadersForFile:(NSString*)directory withName:(nullable NSString*)name withURL:(NSString*)url;

+ (void)fetchDataWithDirectory:(NSString*)directory withName:(nullable NSString*)name withURL:(NSString*)url withCallback:(void(^)(NSData*))callback;
+ (void)fetchDataWithDirectoryReplay:(NSString*)directory withName:(nullable NSString*)name withURL:(NSString*)url withCallback:(void(^)(NSData*, NSString*))callback;
+ (void)preloadImageView:(UIImageView*)imageView withDirectory:(NSString*)directory withName:(nullable NSString*)name withURL:(NSString*)url;

+ (void)preloadButtonBackground:(UIButton *)button withDirectory:(NSString *)directory withname:(NSString *)name withURL:(NSString *)url;

+ (TSGRoute*)parseRoute:(NSDictionary*)routeData withRouteKey:(NSString*)routeKey;

+ (BOOL)tourIsDownloaded:(TSGDRO*)route needsUpdate:(BOOL*)needsUpdate;

+ (void)fetchRouteObjectForDRO:(TSGDRO*)route withLocals:(BOOL)allowLocalUse withCallback:(void(^)(TSGRoute*, BOOL))callback;
+ (void)getRouteDownloadSize:(TSGRoute*)route withCallback:(void(^)(unsigned long long))callback;

@end

NS_ASSUME_NONNULL_END

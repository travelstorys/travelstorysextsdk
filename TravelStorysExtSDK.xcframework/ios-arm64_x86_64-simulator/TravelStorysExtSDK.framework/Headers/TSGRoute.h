//
//  TSGRoute.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/25/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#import <TravelStorysExtSDK/TSGGeotag.h>
#import <TravelStorysExtSDK/TSGTrack.h>
#import <TravelStorysExtSDK/TSGMapOverlay.h>
#import <TravelStorysExtSDK/TSGECoupon.h>
#import <TravelStorysExtSDK/TSGERO.h>

#import <TravelStorysExtSDK/TSGJsonify.h>

NS_ASSUME_NONNULL_BEGIN

/**
 *@brief Full TSGRoute Object.
 *
 */
@interface TSGRoute : NSObject <TSGJsonable>

/**
 *@brief Internal TSG TSGRoute ID
 *
 */
@property (nonatomic) int routeId;

/**
 *@brief Unique alphanumeric route key.
 *
 */
@property (nonatomic) NSString* routeKey;

/**
 *@brief TSG ID for the primary org on the route.
 *
 */
@property (nonatomic) int orgID;

/**
 *@brief Name of the primary org on the route.
 *
 */
@property (nonatomic) NSString* orgString;

/**
 *@brief Array of all orgs' IDs on the route
 *
 */
@property (nonatomic) NSArray* allOrgs;

/**
 *@brief TSG ID for the primary sponsor on the route.
 *
 */
@property (nonatomic) int sponsorID;

/**
 *@brief Name of the primary sponsor on the route.
 *
 */
@property (nonatomic) NSString* sponsorString;

/**
 *@brief Title of the route
 *
 */
@property (nonatomic) NSString* title;

/**
 *@brief Subtitle of the route
 *
 */
@property (nonatomic) NSString* subtitle;

/**
 *@brief State/Country where the route is located.
 *
 */
@property (nonatomic) NSString* state;

/**
 *@brief Transit type of the route.
 *@details See TSGConstants.h for acceptable values
 *
 */
@property (nonatomic) TSGRouteType type;

/**
 *@brief True if the route is active
 *
 */
@property (nonatomic) BOOL live;

/**
 *@brief Release status of the route.
 *@details See TSGConstants.h for acceptable values
 *
 */
@property (nonatomic) ReleaseState releaseState;

/**
 *@brief Minimum app/sdk version the route requires
 *@details For SDK usage, this is based on the SDK version.
 *
 */
@property (nonatomic) NSString* minVersion;

/**
 *@brief True if donation language and icons should be used.
 *@details If this is true, show the gift box icon and use donation
 * language. If false, use connect icon and connect language.
 *
 */
@property (nonatomic) BOOL donate;

/**
 *@brief Link URL for donation/connect CTA
 *
 */
@property (nonatomic) NSString* donateLink;

/**
 *@brief ID of a linked route. This is for future functionality and
 * should not be used at this time.
 *
 */
@property (nonatomic) int linkedRoute;

/**
 *@brief Estimated length of the route, in minutes.
 *
 */
@property (nonatomic) int tourLength;

/**
 *@brief Formatted length of the route with time descriptors.
 *
 */
@property (nonatomic) NSString* lengthString;

/**
 *@brief Summary of the route.
 *
 */
@property (nonatomic) NSString* summary;

/**
 *@brief ID of the route's about geotag.
 *
 */
@property (nonatomic) int aboutTag;

/**
 *@brief True if the route is premium
 *
 */
@property (nonatomic) BOOL paid;

/**
 *@brief Passcode used to enter the route without restriction.
 *
 */
@property (nonatomic) NSString* passcode;

/**
 *@brief URL of the route's discovery view image.
 *
 */
@property (nonatomic) NSString* discoveryImage;

/**
 *@brief Approximate center point of the route.
 *
 */
@property (nonatomic) CLLocationCoordinate2D coordinate;

/**
 *@brief Distance to the center point of the route, in meters.
 *
 */
@property (nonatomic) float distance;

/**
 *@brief Array of all Geotags on the route.
 *
 */
@property (nonatomic) NSArray<TSGGeotag*>* geotags;

/**
 *@brief Array of all Tracks on the route.
 *
 */
@property (nonatomic) NSArray<TSGTrack*>* tracks;

/**
    @brief Array of all Map Overlays on route
 */
@property (nonatomic) NSArray<TSGMapOverlay*>* mapOverlays;

@property (nonatomic) NSArray<TSGECoupon*>* ecoupons;

/**
 *@brief True if the tour has advertising banners/interstitials
 *
 */
@property (nonatomic) BOOL enableAds;

/**
 *@brief TSG ID of the org which is advertising
 *
 */
@property (nonatomic) int adOrg;

/**
 *@brief URL of the ad banner foreground image
 *
 */
@property (nonatomic) NSString* adBanner;

/**
 *@brief URL of the ad banner background image
 *
 */
@property (nonatomic) NSString* adBannerBg;

/**
 *@brief URL of the ad intersitial background image.
 *
 */
@property (nonatomic) NSString* adInterstitialBg;

/**
 *@brief URL of the ad interstitial foreground image.
 *
 */
@property (nonatomic) NSString* adInterstitialFg;

/**
 *@brief CTA URL for the ad.
 *
 */
@property (nonatomic) NSString* adURL;

/**
 *@brief Zoom level at which geotags should show/hide labels.
 *
 */
@property (nonatomic) float zoomScale;

/**
 *@brief Additional information about the route.
 *
 */
@property (nonatomic) NSString* flags;

/**
 *@brief Array of assets for the route.
 *
 */
@property (nonatomic) NSDictionary* assets;

/**
 *@brief Array of music for the route.
 *
 */
@property (nonatomic) NSArray* music;

/**
 *@brief UUID for associated iBeacon Region
 *
 */
@property (nonatomic) NSString* uuid;
@property (nonatomic) int beaconDistance;

/**
 *@brief Type of display mode
 */
@property (nonatomic) NSString* displayMode;

/**
 *@brief Indoor Map Data
 */
@property (nonatomic) NSArray* indoorMaps;

@property (nonatomic) BOOL interrupt;

@property (nonatomic) NSString* keywords;
@property (nonatomic) NSString* dynamicLink;

@property (nonatomic) BOOL private;
@property (nonatomic) NSArray* secondaryOrgs;

- (TSGERO*)createERO;

@end

NS_ASSUME_NONNULL_END

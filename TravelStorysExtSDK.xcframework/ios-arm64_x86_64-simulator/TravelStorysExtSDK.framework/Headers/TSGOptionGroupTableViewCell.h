//
//  TSGOptionGroupTableViewCell.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 1/27/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum TSGOptionGroupType : UInt8
{
    GroupTypeNoCorners,
    GroupTypeTopCorners,
    GroupTypeBottomCorners,
    GroupTypeAllCorners
} TSGOptionGroupType;

@protocol TSGOptionGroupTableViewCellDelegate

- (void)didSelectOptionGroupTableViewCell:(NSIndexPath*)indexPath;

@end

@interface TSGOptionGroupTableViewCell : UITableViewCell
{
    IBOutlet NSLayoutConstraint* topConstraint;
    IBOutlet NSLayoutConstraint* bottomConstraint;
    
    IBOutlet UIImageView* iconView;
    IBOutlet UILabel* titleLabel;
    
    IBOutlet UIView* topCornerView;
    IBOutlet UIView* bottomCornerView;
    IBOutlet UIView* noCornerView;
    IBOutlet UIView* noCornerInnerView;
    
    IBOutlet UIButton* button;
    
}

@property (nonatomic) id <TSGOptionGroupTableViewCellDelegate> delegate;
@property (nonatomic) NSIndexPath* indexPath;

- (void)loadWithType:(TSGOptionGroupType)type withTitle:(NSString*)title withIcon:(UIImage*)icon;

- (void)setEnabled:(BOOL)enabled;

@end

NS_ASSUME_NONNULL_END

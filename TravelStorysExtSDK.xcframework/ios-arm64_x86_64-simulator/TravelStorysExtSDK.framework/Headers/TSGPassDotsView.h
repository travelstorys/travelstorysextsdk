//
//  TSGPassDotsView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

IB_DESIGNABLE
@interface TSGPassDotsView : UIView

@property (nonatomic) IBInspectable int numDots;
@property (nonatomic) IBInspectable int filledDots;
@property (nonatomic) IBInspectable int padding;

@end

NS_ASSUME_NONNULL_END

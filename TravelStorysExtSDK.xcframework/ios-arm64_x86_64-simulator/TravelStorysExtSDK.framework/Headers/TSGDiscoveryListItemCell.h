//
//  TSGDiscoveryListItemCell.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/30/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreText/CoreText.h>
#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGBoilerViewSupport.h>
#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDataManager.h>
#import <TravelStorysExtSDK/TSGDownloadManager.h>
#import <TravelStorysExtSDK/TSGERO.h>
#import <TravelStorysExtSDK/TSGShorthand.h>
//#import <TravelStorysExtSDK/TSGFlatProgressView.h>
#import <TravelStorysExtSDK/TSGDiscoveryListItem.h>

#import <TravelStorysExtSDK/TSGCircleProgressView.h>

#import <TravelStorysExtSDK/TSGPurchaseViewController.h>

#import <TravelStorysExtSDK/TSGNSString+Hash.h>

NS_ASSUME_NONNULL_BEGIN

@class TSGDiscoveryListItemCell;
@protocol TSGDiscoveryListItemCellDelegate

- (void)previewCellItem:(TSGDiscoveryListItemCell*)item;
- (void)playCellItem:(TSGDiscoveryListItemCell*)item;
- (void)openPrivateCellItem:(TSGDiscoveryListItemCell*)item;

@end

@interface TSGDiscoveryListItemCell : UITableViewCell <TSGPurchaseDelegate>
{
    IBOutlet UIImageView* discImageView;
    IBOutlet UILabel* titleLabel;
    IBOutlet UILabel* subtitleLabel;
    IBOutlet UILabel* stateLabel;
    IBOutlet UILabel* distanceLabel;
    IBOutlet UIImageView* typeIcon;
    
    IBOutlet UIButton* coverButton;
    IBOutlet UIButton* actionButton;
    IBOutlet UIButton* privateButton;
    
    IBOutlet UILabel* comingSoonLabel;
    IBOutlet UILabel* updateRequiredLabel;
    
    TSGDownloadTaskGroup* taskGroup;
    
    IBOutlet UIActivityIndicatorView* activity;
    
    IBOutlet UIImageView* premiumBackground;
    IBOutlet UILabel* premiumLabel;
    
    IBOutlet UIVisualEffectView* downloadingView;
    IBOutlet UIProgressView* progressView;
    IBOutlet UILabel* downloadingTitleLabel;
    IBOutlet UILabel* downloadingSizeLabel;
    IBOutlet UIActivityIndicatorView* downloadingActivityView;
}

@property (nonatomic) TSGERO* route;
@property (nonatomic) id<TSGDiscoveryListItemCellDelegate> delegate;

@property (atomic) BOOL downloadChecked;
@property (atomic) NSMutableArray* dliNeedsUpdate;

- (void)setDistance:(float)meters;
- (void)refresh;
- (void)reset;

@end

NS_ASSUME_NONNULL_END

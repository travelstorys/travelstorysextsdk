//
//  TSGECoupon.h
//  TravelStorysExtSDK
//
//  Created by Matt Ellison on 3/26/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TravelStorysExtSDK/TSGJsonify.h>

#ifndef TSGECoupon_h
#define TSGECoupon_h

@interface TSGECoupon : NSObject <TSGJsonable>

@property int couponID;
@property int routeID;
@property int geotagID;
@property NSString* title;
@property NSString* couponDescription;
@property NSString* url;

@end

#endif /* TSGECoupon.h */

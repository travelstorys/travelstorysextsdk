//
//  TSGSessionManagerV2.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 11/5/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <TravelStorysExtSDK/TravelStorysExtSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGSessionManagerV2 : TSGSessionManager

+ (TSGSessionManagerV2*)sharedV2;

@end

NS_ASSUME_NONNULL_END

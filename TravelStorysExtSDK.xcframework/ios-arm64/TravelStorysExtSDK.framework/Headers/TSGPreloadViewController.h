//
//  TSGPreloadViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/19/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TravelStorys.h>
#import <TravelStorysExtSDK/TSGSessionManager.h>

#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGPermissionsViewController.h>

#import <TravelStorysExtSDK/TSGLoadingAnimationView.h>
#import <TravelStorysExtSDK/TSGFlatProgressView.h>

#import <TravelStorysExtSDK/TSGShorthand.h>
#import <TravelStorysExtSDK/Reachability.h>

#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGPreloadViewController : UIViewController<ITSGViewController, TSGSessionManagerDelegate, UIAlertViewDelegate>

@property (nonatomic, assign) IBOutlet TSGLoadingAnimationView* animation;
@property (nonatomic, assign) IBOutlet TSGFlatProgressView* progress;
@property (nonatomic, assign) IBOutlet UIImageView* logoView;
@property (nonatomic, assign) IBOutlet UILabel* mainLabel;
@property (nonatomic, assign) IBOutlet UILabel* subLabel;

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

- (void)startPreload;
- (void)completeView;

@end

NS_ASSUME_NONNULL_END

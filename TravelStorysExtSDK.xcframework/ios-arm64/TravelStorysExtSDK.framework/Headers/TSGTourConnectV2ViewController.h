//
//  TSGTourConnectV2ViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 1/21/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import <TravelStorysExtSDK/TSGBoilerViewSupport.h>
#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDataManager.h>
#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGOrg.h>
#import <TravelStorysExtSDK/TSGShorthand.h>
#import <TravelStorysExtSDK/TSGSocialManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGTourConnectV2ViewController : UIViewController <
    MFMessageComposeViewControllerDelegate,
    ITSGViewController
>

@property IBOutlet UIImageView* backgroundImageView;
@property IBOutlet UIImageView* orgLogoImageView;

@property IBOutlet UILabel* orgTitleLabel;
@property IBOutlet UITextView* connectBodyText;

@property IBOutlet UIButton* actionButton;

@property IBOutlet UIButton* closeButton;
@property IBOutlet UIView* swipeInterceptView;

@property (nonatomic, readonly) TSGOrg* org;

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

- (void)loadOrg:(TSGOrg*)org fromRoute:(TSGRoute*)route withTrack:(TSGTrack*)track;
- (void)close;

@end

NS_ASSUME_NONNULL_END

//
//  TSGSmallSlider.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/5/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TSGSmallSliderDelegate <NSObject>

- (void)softUpdate:(float)progressValue;
- (void)hardUpdate:(float)progressValue;

@end

@interface TSGSmallSlider : UIView
{
    IBOutlet UIView* backTrack;
    IBOutlet UIView* frontTrack;
    IBOutlet UIView* thumb;
    
    BOOL isDragging;
    CGPoint startPoint;
    float offset;
    
    UITouch* lastTouch;
}

@property (nonatomic) float progress;
@property (nonatomic) IBOutlet id<TSGSmallSliderDelegate> delegate;

@end
NS_ASSUME_NONNULL_END

//
//  TSGCRO.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/27/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <TravelStorysExtSDK/TSGJsonify.h>

NS_ASSUME_NONNULL_BEGIN

/**
 *@brief Collapsed TSGRoute Object is the smallest version of a route object used
 * for keeping references to all route objects in memory.
 *
 */
@interface TSGCRO : NSObject <NSCoding, TSGJsonable>
/**
 *@brief Internal TSG TSGRoute ID
 *
 */
@property (nonatomic) int routeId;
/**
 *@brief Unique alphanumeric route key.
 *
 */
@property (nonatomic) NSString* routeKey;

/**
 *@brief UTC of last modification.
 *
 */
@property (nonatomic) unsigned long long modified;

@end

NS_ASSUME_NONNULL_END

//
//  TSGCircleOutlineProgressView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 11/4/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

IB_DESIGNABLE
@interface TSGCircleOutlineProgressView : UIView

@property (nonatomic) IBInspectable double progress;
@property (nonatomic) IBInspectable int lineWidth;
@property (nonatomic) IBInspectable UIColor* backColor;
@property (nonatomic) IBInspectable UIColor* foreColor;

@end

NS_ASSUME_NONNULL_END

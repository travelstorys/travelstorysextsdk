//
//  TSGFlatProgressView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/20/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGFlatProgressView : UIView

@property (nonatomic) float progress;

- (void)setForegroundColor:(UIColor*)color;

@end

NS_ASSUME_NONNULL_END

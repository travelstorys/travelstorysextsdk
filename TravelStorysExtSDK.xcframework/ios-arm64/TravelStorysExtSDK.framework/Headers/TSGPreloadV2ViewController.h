//
//  TSGPreloadV2ViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 11/4/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGCircleOutlineProgressView.h>
#import <TravelStorysExtSDK/TSGSessionManagerV2.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGPreloadV2ViewController : UIViewController <TSGSessionManagerDelegate>

@property (nonatomic) IBOutlet UILabel* statusLabel;
@property (nonatomic) IBOutlet TSGCircleOutlineProgressView* progressView;

@property (nonatomic) IBOutlet UILabel* versionLabel;
@property (assign) IBOutlet UIImageView* imageView;

@end

NS_ASSUME_NONNULL_END

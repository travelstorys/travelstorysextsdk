//
//  TSGAudioManager.h
//  TravelStorys
//
//  Created by David Worth on 7/4/17.
//  Copyright © 2017 TravelStorysGPS. All rights reserved.
//

/*
 * Contains the code for the global
 * Audio Manager
 */

#import <Foundation/Foundation.h>

#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

#import <TravelStorysExtSDK/TSGAudioManagerSubscriber.h>
#import <TravelStorysExtSDK/TSGShorthand.h>

#define kNotificationAudioStop @"kNotificationAudioStop"

/**
 * @protocol AudioDelegateListener
 *
 * @brief AudioDelegateListeners receive updates
 *  when the audio controls should be updated.
 *
 */
@protocol TSGAudioDelegateListener <NSObject>

/**
 * @brief called when audio controls should be
 * updated.
 *
 * @param enable
 *  YES if the controls should be shown. No Otherwise.
 *
 */
+ (void)updateControlsFromAudio:(BOOL)enable;

@end

@interface TSGAudioManager : NSObject <AVAudioPlayerDelegate>
{
    //Audio Queue
    NSMutableArray<NSDictionary*>* queue;
    
    //Audio Manager Subscribers
    NSMutableArray<NSObject<TSGAudioManagerSubscriber>*>* subscribers;
    
    //Update timer
    CADisplayLink* timer;
    
    //Current queue index
    int queueIndex;
    
    //Push Audio Title
    NSString* pushTitle;
    
    //Push Audio Subtitle
    NSString* pushSubtitle;
    
    //Push Audio Lock (block push audio)
    BOOL pushLock;
}

/**
 * @property playMode
 *  TSGAudioPlayMode
 *
 * @brief contains the current audio play mode
 *
 */
@property (nonatomic, readonly) TSGAudioPlayMode playMode;

/**
 * @property isPlaying
 *  BOOL
 *
 * @brief YES if audio is currently playing
 *
 */
@property (nonatomic, readonly) BOOL isPlaying;

/**
 * @property audioDelegate
 *  id<AudioDelegateListener>
 *
 * @brief Audio Controls update listener
 *
 */
@property (nonatomic, assign) Class<TSGAudioDelegateListener> audioDelegate;

/**
 * @property queuePlayer
 *  AVAudioPlayer
 *
 * @brief The audio player for queue audio
 *
 */
@property (nonatomic, retain) AVAudioPlayer* queuePlayer;

/**
 * @property pushPlayer
 *  AVAudioPlayer
 *
 * @brief The audio player for pushed audio
 *
 */
@property (nonatomic, retain) AVAudioPlayer* pushPlayer;

/**
 * @brief returns the shared audio manager
 *
 * @discussion if the shared audio manager
 *  does not exist, creates a new one.
 *
 * @return
 *  returns the shared Audio Manager
 *
 */
+ (TSGAudioManager*)sharedManager;

/**
 * @brief clears the audio queue
 *
 */
- (void)clearQueue;

/**
 * @brief clears all AudioManagerSubscribers
 *
 * @see TSGAudioManagerSubscriber
 *
 */
- (void)clearSubscribers;

/**
 * @brief adds audio file to the queue.
 *
 * @discussion adds an audio file to the background
 *  playback queue.
 *
 * @param audioData
 *  The raw audio data to load
 *
 * @param title
 *  The title of the audio file
 *
 * @param subtitle
 *  The subtitle or artist of the audio file
 *
 */
- (void)queueAudio:(NSData*)audioData withTitle:(NSString*)title withSubtitle:(NSString*)subtitle;


/**
 * @brief pushes audio file to the queue.
 *
 * @discussion pushes an audio file to the foreground
 *  and plays immediately.
 *
 * @param audioData
 *  The raw audio data to load
 *
 * @param title
 *  The title of the audio file
 *
 * @param subtitle
 *  The subtitle or artist of the audio file
 *
 */
- (void)pushAudio:(NSData*)audioData withTitle:(NSString*)title withSubtitle:(NSString*)subtitle;

/**
 * @brief adds an TSGAudioManagerSubscriber to the list.
 *
 */
- (void)subscribe:(NSObject<TSGAudioManagerSubscriber>*)subscriber;

/**
 * @brief removes an TSGAudioManagerSubscriber from the list.
 *
 */
- (void)unsubscribe:(NSObject<TSGAudioManagerSubscriber>*)subscriber;

/**
 * @brief plays current audio
 *
 */
- (void)play;

/**
 * @brief pauses current audio
 *
 */
- (void)pause;

/**
 * @brief stops current audio
 *
 */
- (void)stop;
/**
 * @brief skips to the next audio file
 *
 */
- (void)next;

/**
 * @brief rewinds the audio
 *
 * @discussion if the audio is more than 5 seconds in,
 * it will rewind to the beginning of the file. If it is less
 * than 5 seconds in, it will move to the previous file or stop
 * the audio if none is available.
 *
 */
- (void)prev;

/**
 * @brief returns the current push audio title
 *
 * @return
 *  the current push audio title
 *
 */
- (NSString*)getPushTitle;

/**
 * @brief returns the current push audio subtitle
 *
 * @return
 *  the current push audio subtitle
 *
 */
- (NSString*)getPushSubtitle;

/**
 * @brief gets the active AVAudioPlayer
 *
 * @return
 *  Returns the actively playing AVAudioPlayer.
 *
 */
- (AVAudioPlayer*)activePlayer;

- (BOOL)getPlaying;

@end

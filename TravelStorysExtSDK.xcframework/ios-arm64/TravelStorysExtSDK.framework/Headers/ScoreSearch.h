//
//  ScoreSearch.h
//  ScoreSearch
//
//  Created by David Worth on 9/11/17.
//  Copyright © 2017 TravelStorysGPS. All rights reserved.
//

#import <TravelStorysExtSDK/SSConstants.h>
#import <TravelStorysExtSDK/SSCore.h>
#import <TravelStorysExtSDK/SSScoringIndex.h>

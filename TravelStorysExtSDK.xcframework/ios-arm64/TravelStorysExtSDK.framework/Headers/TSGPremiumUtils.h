//
//  PremiumUtils.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/12/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <TravelStorysExtSDK/TravelStorys.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGPremiumUtils : NSObject

+ (void)updateLocalPurchaseInfo:(NSString*)tourKey withStartDate:(NSDate*)date withLifetime:(int)lifetime;
+ (int)getOwnership:(NSString*)tourKey;
+ (NSTimeInterval)getRemainingOwnership:(NSString*)tourKey;
+ (NSArray*)getOwnedTours;

+ (void)remoteUpdateAllPurchases:(void(^)(void))onComplete;

+ (void)clearAllPurchaseInfo;

@end

NS_ASSUME_NONNULL_END

//
//  TSGTrack.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/26/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <TravelStorysExtSDK/TSGJsonify.h>

NS_ASSUME_NONNULL_BEGIN

/**
 *@brief Container for TSG TSGTrack objects.
 *
 */
@interface TSGTrack : NSObject <TSGJsonable>

/**
 *@brief Unique TSG TSGRoute ID this track belongs to.
 *
 */
@property (nonatomic) int routeID;

/**
 *@brief Unique TSG TSGTrack ID for this track.
 *
 */
@property (nonatomic) int trackID;

/**
 *@brief True if this track is active.
 *
 */
@property (nonatomic) BOOL live;

/**
 *@brief Title of the track.
 *
 */
@property (nonatomic) NSString* title;

/**
 *@brief Summary of the track.
 *
 */
@property (nonatomic) NSString* descriptionText;

/**
 *@brief Name of the custom icon for the track.
 *
 */
@property (nonatomic) NSString* icon;

/**
 *@brief Legend contents of the KML file for this track.
 *
 */
@property (nonatomic) NSString* kmlBanner;

/**
 *@brief Asset ID of a custom intro for this track.
 *
 */
@property (nonatomic) int introID;

/**
 *@brief Asset ID of a custom second intro for this track.
 *
 */
@property (nonatomic) int secondIntroID;

/**
 *@brief Id of the about tag for this track.
 *
 */
@property (nonatomic) int aboutTag;

/**
 *@brief Org id of the org for this track.
 *
 */
@property (nonatomic) int org;

/**
 @brief Whether or not to override the connect info.
 */
@property (nonatomic) BOOL overrideConnect;

/**
 @brief Connect action button text. Only used if @overrideConnect is true.
 */
@property (nonatomic) NSString* connectAction;

/**
 @brief Connect banner text. Only used if @overrideConnect is true.
 */
@property (nonatomic) NSString* connectBanner;

/**
 @brief Connect body text. Only used if @overrideConnect is true.
 */
@property (nonatomic) NSString* connectBody;

/**
 @brief Connect name text. Only used if @overrideConnect is true.
 */
@property (nonatomic) NSString* connectName;

@end

NS_ASSUME_NONNULL_END

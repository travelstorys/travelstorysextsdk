//
//  TSGDiscoveryViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/14/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TravelStorys.h>

#import <TravelStorysExtSDK/TSGConstants.h>
#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDeepLinkHandler.h>
#import <TravelStorysExtSDK/TSGGeotagAnnotation.h>
#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/ScoreSearch.h>
#import <TravelStorysExtSDK/TSGShorthand.h>

#import <TravelStorysExtSDK/TSGCircleActivityView.h>
#import <TravelStorysExtSDK/TSGDiscoveryListItem.h>
#import <TravelStorysExtSDK/TSGDiscoveryListItemCell.h>

#import <TravelStorysExtSDK/TSGLoginViewController.h>
#import <TravelStorysExtSDK/TSGModalWebViewController.h>
#import <TravelStorysExtSDK/TSGRatingViewController.h>

#import <TravelStorysExtSDK/TSGPasscodeViewController.h>

#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGDiscoveryViewController : UIViewController <
CLLocationManagerDelegate,
TSGDiscoveryListItemDelegate,
TSGDiscoveryListItemCellDelegate,
ITSGViewController,
MKMapViewDelegate,
UITextFieldDelegate,
UIScrollViewDelegate,
TSGLoginDelegate,
TSGDeepLinkResponder,
TSGSyncDelegate,
UITableViewDataSource,
UITableViewDelegate,
TSGPasscodeDelegate
>
{
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIView* titleBackingView;
    IBOutlet UIButton* titleMapButton;
    IBOutlet UIButton* titleAccountButton;
    IBOutlet UIButton* titleSearchButton;
    IBOutlet UIButton* titleMoreButton;
    
    IBOutlet UIView* searchPanel;
    IBOutlet UITextField* searchField;
    IBOutlet NSLayoutConstraint* topSearchPanelConstraint;
    IBOutlet NSLayoutConstraint* bottomScrollViewConstraint;
    
    IBOutlet UIScrollView* scrollView;
    IBOutlet UITableView* tableView;
    
    IBOutlet TSGCircleActivityView* activityView;
    
    IBOutlet MKMapView* mapView;
    
    CLLocationManager* locationManager;
    NSMutableArray<TSGERO*>* eros;
    NSMutableArray<TSGERO*>* ownedEros;
    NSMutableArray<NSNumber*>* ownedIndices;
    
    IBOutlet UIView* bandwidthMessage;
    IBOutlet NSLayoutConstraint* bandwidthTopConstraint;
}

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

+ (UIImage*)backgroundImage;
- (void)mapSelect:(RouteButton*)sender;
- (void)showMoreOptions;

- (IBAction)hideLowBandwidth:(id)sender;

@end

NS_ASSUME_NONNULL_END

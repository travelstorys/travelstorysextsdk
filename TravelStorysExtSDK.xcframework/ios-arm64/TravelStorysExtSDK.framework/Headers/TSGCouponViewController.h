//
//  TSGCouponViewController.h
//  TravelStorysExtSDK
//
//  Created by Matt Ellison on 3/27/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGConstants.h>
#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGShorthand.h>
#import <TravelStorysExtSDK/TSGECoupon.h>
#import <TravelStorysExtSDK/TSGRoute.h>
#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGCouponViewController : UIViewController <ITSGViewController>
{
    IBOutlet UIImageView* couponImage;
    IBOutlet UIButton* close;
    IBOutlet UIButton* redeemButton;
    IBOutlet UIButton* notNowButton;
    
    IBOutlet UILabel* cTitle;
    IBOutlet UILabel* timerLabel;
    IBOutlet UITextView* description;
    int seconds;
}

- (void)loadCoupon:(TSGECoupon *)coupon withRoute:(TSGRoute *)route;
- (void)close;
- (void)startTimer;
- (void)setTimer;
- (void)approve;


@property (nonatomic) NSTimer *countdownTimer;
@property (nonatomic) TSGECoupon *coupon;
@property (nonatomic) TSGRoute *route;


@end

NS_ASSUME_NONNULL_END

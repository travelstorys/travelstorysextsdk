//
//  TSGSectionHeaderFooterView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 8/18/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGSectionHeaderFooterView : UITableViewHeaderFooterView

@property (assign) IBOutlet UILabel* contentLabel;

+ (NSString*)reuseIdentifier;
+ (UINib*)getNib;

@end

NS_ASSUME_NONNULL_END

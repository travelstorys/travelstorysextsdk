//
//  TravelStorys.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 2/22/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum TravelStorysStatus : UInt8
{
    StatusConnected,
    StatusNotConnected
} TravelStorysStatus;

typedef enum TravelStorysLogin : UInt8
{
    LoggedInTravelStorys,
    LoggedInFacebook,
    LoggedInApple,
    LoggedInTemporary,
    NotLoggedIn
} TravelStorysLogin;

@interface TravelStorys : NSObject

+ (void)setAPIKey:(NSString*)apiKey;
+ (void)setAPISecret:(NSString*)apiSecret;

+ (void)connect:(void(^)(BOOL))callback;
+ (void)checkForUpdates:(void(^)(int))callback;

+ (void)refreshAllRoutes:(void(^)(NSArray<NSDictionary*>*))callback;
+ (void)refreshDROS:(void(^)(NSArray<NSDictionary*>*))callback;
+ (NSDictionary*)getRoute:(NSString*)routeKey;
+ (NSDictionary*)getRoutes:(NSArray<NSString*>*)routeKeys;

+ (void)saveLoginInfoLink:(int)uid withMode:(TravelStorysLogin)loginMode withEmail:(NSString*)email;

+ (BOOL)loginWithEmail:(NSString*)email withPassword:(NSString*)password isFacebook:(BOOL)facebook;
+ (int)registerWithEmail:(NSString*)email withPassword:(NSString*)password isFacebook:(BOOL)facebook withFirstName:(NSString*)first withLastName:(NSString*)lastName;
+ (int)registerWithApple:(NSString*)email withFirstName:(NSString*)first withLastName:(NSString*)last withUUID:(NSString*)uuid;
+ (BOOL)loginWithApple:(NSString*)uuid;
+ (BOOL)loginWithLink:(NSString *)email withUID:(int)uid withTravelStorys:(TravelStorysLogin)status withAuth:(NSString*)auth;
+ (NSDictionary*)validateTemporaryID;
+ (BOOL)verifyAccount;
+ (BOOL)deviceIsAssigned;

+ (NSDictionary*)getHotelCustomization:(NSString*)hotelId;

+ (void)reportFeedback:(int)feedback withEmail:(NSString*)email withMessage:(NSString*)message;

+ (NSDictionary*)getProductInfo:(int)tourID;
+ (NSDictionary*)getPurchaseCode:(NSString*)code;

+ (TravelStorysLogin)getLoginStatus;
+ (void)logout;

+ (void)setFacebookEnabled:(BOOL)facebookEnabled;

+ (NSDictionary*)reportEvent:(NSString*)appName
   withSession:(NSString*)session
        withOS:(NSString*)os
 withOSVersion:(NSString*)osVersion
     withModel:(NSString*)model
withAppVersion:(NSString*)appVersion
  withCategory:(NSString*)category
    withAction:(NSString*)action
     withLabel:(NSString*)label
     withValue:(double)value
      withData:(NSString*)data
     withData2:(NSString*)data2
      withUUID:(NSString*)uuid
      withIDFA:(NSString*)idfa
       withLat:(double)lat
       withLng:(double)lng;

@end

NS_ASSUME_NONNULL_END

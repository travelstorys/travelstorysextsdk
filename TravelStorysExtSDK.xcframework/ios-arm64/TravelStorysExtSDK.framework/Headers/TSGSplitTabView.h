//
//  TSGSplitTabView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/13/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class TSGSplitTabView;

@protocol TSGSplitTabViewDelegate <NSObject>

- (void)tabChanged:(TSGSplitTabView*)sender withIndex:(int)index;

@end

IB_DESIGNABLE
@interface TSGSplitTabView : UIView
{
    NSArray<UIButton*>* buttonArray;
}

@property (nonatomic) IBInspectable int buttons;
@property (nonatomic) IBInspectable NSString* buttonTitlesCSV;
@property (nonatomic) IBInspectable int selectedButton;

@property (nonatomic) IBOutlet id<TSGSplitTabViewDelegate> delegate;

@end

NS_ASSUME_NONNULL_END

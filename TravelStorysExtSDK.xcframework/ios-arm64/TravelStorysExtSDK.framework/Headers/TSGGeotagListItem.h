//
//  TSGGeotagListItem.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/21/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <TravelStorysExtSDK/TSGRoute.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGGeotagListItem : UITableViewCell
{
    IBOutlet UIView* aView1;
    IBOutlet UIView* aView2;
    IBOutlet UIView* aView3;
}

typedef enum TSGGeotagListItemCallbackMode : UInt8 {
    GeotagListItemCallbackDirections,
    GeotagListItemCallbackLocation
} TSGGeotagListItemCallbackMode;

typedef void (^TSGGeotagListItemCallback)(int geotagId, TSGGeotagListItemCallbackMode mode);

@property IBOutlet UIImageView* iconView;
@property IBOutlet UILabel* titleLabel;
@property IBOutlet UIButton* findButton;
@property IBOutlet UIButton* directionsButton;
@property IBOutlet UIImageView* disclosureImage;

@property IBOutlet NSLayoutConstraint* directionsRightConstraint;

@property (atomic) int loadedGeotag;
@property (readonly) BOOL animating;

@property (atomic) TSGGeotagListItemCallback callback;

- (void)startAnimation;
- (void)stopAnimation;
- (void)loadGeotag:(TSGGeotag*)geotag inRoute:(TSGRoute*)route stripe:(BOOL)stripe;

@end

NS_ASSUME_NONNULL_END

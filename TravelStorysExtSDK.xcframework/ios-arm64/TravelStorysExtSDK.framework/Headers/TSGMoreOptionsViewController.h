//
//  TSGMoreOptionsViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/15/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGBoilerViewSupport.h>
#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDataManager.h>
#import <TravelStorysExtSDK/ITSGViewController.h>
#import <TravelStorysExtSDK/TSGLoginViewController.h>
#import <TravelStorysExtSDK/TSGSyncViewController.h>
#import <TravelStorysExtSDK/TSGManageViewController.h>
#import <TravelStorysExtSDK/TSGPasscodeViewController.h>
#import <TravelStorysExtSDK/TSGPreviewViewController.h>

#import <TravelStorysExtSDK/TSGIconListItem.h>
#import <TravelStorysExtSDK/TSGIconListView.h>

#import <TravelStorysExtSDK/TSGShorthand.h>

#import <TravelStorysExtSDK/TSGAnalyticsManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGMoreOptionsViewController : UIViewController <ITSGViewController, TSGIconListViewDelegate, TSGPasscodeDelegate, TSGLoginDelegate, TSGSyncDelegate>
{
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIButton* titleShareButton;
    
    IBOutlet TSGIconListView* listView;
    
    IBOutlet UIImageView* backgroundImageView;
    
    TSGIconListItem* loginItem;
    TSGIconListItem* syncItem;
}

@property id<ITSGViewControllerDelegate> delegate;

- (void)close;
- (void)getPreviewControllerForRoute:(TSGERO*)route;

@end

NS_ASSUME_NONNULL_END

//
//  TSGGeotagAnnotation.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/29/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <MapKit/MapKit.h>

#import <TravelStorysExtSDK/TSGConstants.h>
#import <TravelStorysExtSDK/TSGDRO.h>
#import <TravelStorysExtSDK/TSGERO.h>
#import <TravelStorysExtSDK/TSGGeotag.h>
#import <TravelStorysExtSDK/TSGRoute.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGGeotagAnnotation : MKPointAnnotation

@property BOOL touchable;
@property TSGGeotagPin pinImage;
@property TSGGeotag* geotag;
@property TSGGeotagTriggerMode triggerMode;

@end

@interface TSGGeotagStackAnnotation : MKPointAnnotation

@property int stackId;
@property NSArray<TSGGeotag*>* geotags;

@end

@interface TSGDRouteAnnotation : MKPointAnnotation

@property int routeId;
@property TSGDRO* route;

@end

@interface TSGRouteAnnotation : MKPointAnnotation

@property int routeId;
@property TSGERO* route;

@end

@interface GeotagButton : UIButton

@property TSGGeotagAnnotation* annotation;

@end

@interface GeotagDirectionsButton : UIButton

@property CLLocationCoordinate2D location;

@end

@interface RouteButton : UIButton

@property TSGRouteAnnotation* annotation;

@end

@interface TSGDRouteButton : UIButton

@property TSGDRouteAnnotation* annotation;

@end

NS_ASSUME_NONNULL_END

//
//  TSGDiscoveryListItemCellV2.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 11/5/20.
//  Copyright © 2020 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreText/CoreText.h>
#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGDRO.h>

#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDataManager.h>

#import <TravelStorysExtSDK/TSGConstants.h>

#import <TravelStorysExtSDK/TSGPurchaseViewController.h>

@class TSGLanguageView;
@class TSGPopoverView;

NS_ASSUME_NONNULL_BEGIN

@class TSGDiscoveryListItemCellV2;
@protocol TSGDiscoveryListItemCellV2Delegate

- (void)previewCellItem:(TSGDiscoveryListItemCellV2*)item;
- (void)playCellItem:(TSGDiscoveryListItemCellV2*)item;
- (void)openPrivateCellItem:(TSGDiscoveryListItemCellV2*)item;
- (void)downloadCellItem:(TSGDiscoveryListItemCellV2*)item;

@end

@interface TSGDiscoveryListItemCellV2 : UITableViewCell <TSGDRODownloadDelegate, TSGPurchaseDelegate>
{
    BOOL favorited;
    
    TSGDiscoveryAction action;
}

@property (nonatomic) IBOutlet UIView* container;
@property (nonatomic) IBOutlet UIImageView* tileImageView;
@property (nonatomic) IBOutlet UIImageView* subTileImageView;

@property (nonatomic) IBOutlet UILabel* titleLabel;
@property (nonatomic) IBOutlet UILabel* subtitleLabel;
@property (nonatomic) IBOutlet UILabel* stateLabel;
@property (nonatomic) IBOutlet UILabel* distanceLabel;

@property (nonatomic) IBOutlet UIImageView* typeImage1;
@property (nonatomic) IBOutlet UIImageView* typeImage2;

@property (nonatomic) IBOutlet UIButton* actionButton;
@property (nonatomic) IBOutlet UIButton* favoriteButton;
@property (nonatomic) IBOutlet UIButton* coverButton;

@property (nonatomic) IBOutlet UIImageView* flagImage;
@property (nonatomic) IBOutlet UILabel* flagLabel;

@property (nonatomic) IBOutlet UILabel* comingSoonLabel;

@property (nonatomic) IBOutlet UIVisualEffectView* downloadView;
@property (nonatomic) IBOutlet UILabel* downloadTitleLabel;
@property (nonatomic) IBOutlet UILabel* downloadTypeLabel;
@property (nonatomic) IBOutlet UILabel* downloadProgressLabel;
@property (nonatomic) IBOutlet UIProgressView* downloadProgressView;
@property (nonatomic) IBOutlet UILabel* downloadSpeedLabel;

@property (nonatomic) IBOutlet TSGLanguageView* languageView;
@property (nonatomic) IBOutlet TSGPopoverView* popoverView;
@property (nonatomic) IBOutlet NSLayoutConstraint* popoverTopConstraint;

@property (nonatomic) TSGDRO* route;

@property (assign) id<TSGDiscoveryListItemCellV2Delegate> delegate;

- (void)reset;

- (void)loadAsTutorial;
- (void)cancelTutorial;
- (void)fakeTutorialDownloadFinish;

@end

NS_ASSUME_NONNULL_END

//
//  TSGAnimBubblePointView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/12/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class TSGAnimBubblePointView;

@protocol TSGAnimBubblePointViewDelegate <NSObject>

- (void)bubbleSkipped:(TSGAnimBubblePointView*)view;

@end

@interface TSGAnimBubblePointView : UIView

@property BOOL showsSkipButton;
@property id<TSGAnimBubblePointViewDelegate> delegate;

- (instancetype)initBubbleWithTitle:(NSString*)title
                        withContent:(NSString*)content
                    withTargetPoint:(CGPoint)target
                   withTargetPoint2:(CGPoint)target2
                     usesTwoTargets:(BOOL)twoTargets
                withBackgroundColor:(UIColor*)backgroundColor
                withForegroundColor:(UIColor*)foregroundColor
              withBoundingRectangle:(CGRect)bounds
                     withBubbleSize:(CGSize)size
                          favorsTop:(BOOL)top
                         favorsLeft:(BOOL)left;

- (void)showBubbleWithTitle:(NSString*)title
                withContent:(NSString*)content
            withTargetPoint:(CGPoint)target
           withTargetPoint2:(CGPoint)target2
             usesTwoTargets:(BOOL)twoTargets
        withBackgroundColor:(UIColor*)backgroundColor
        withForegroundColor:(UIColor*)foregroundColor
      withBoundingRectangle:(CGRect)bounds
             withBubbleSize:(CGSize)size
                  favorsTop:(BOOL)top
                 favorsLeft:(BOOL)left;

- (void)addBlurWithCutoutRect:(CGRect)rect;
- (void)addBlurWithRect:(CGRect)rect;
- (void)removeBlur;
- (void)removeBubble;

- (CGRect)getBubbleFrame;

@end

NS_ASSUME_NONNULL_END

//
//  TSGTriggerLogic.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/28/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGGeotag.h>
#import <TravelStorysExtSDK/TSGRoute.h>
#import <TravelStorysExtSDK/TSGAudioManager.h>
#import <TravelStorysExtSDK/TSGShorthand.h>
#import <TravelStorysExtSDK/TSGAnalyticsManager.h>


NS_ASSUME_NONNULL_BEGIN

typedef void (^GeotagCallback)(TSGGeotag*);

typedef enum TSGTriggerState : UInt8
{
    TriggerEnabledGeneric,
    TriggerEnabledPrevSet,
    TriggerDisabledGeneric,
    TriggerDisabledNextSet,
    TriggerDisabledPrevSet,
    TriggerDisabledTriggerOnce
} TSGTriggerState;

@interface TSGTriggerMap : NSObject

+ (instancetype)sharedMap;
- (void)clearMap;
- (void)mapGeotags:(NSArray<TSGGeotag*>*)geotags;
- (void)geotagTriggered:(TSGGeotag*)geotag;
- (TSGTriggerState)getGeotagTriggerState:(TSGGeotag*)geotag;

@end

@interface TSGTourMemory : NSObject

+ (instancetype)shared;
- (void)registerGeotag:(TSGGeotag*)geotag;
- (BOOL)checkGeotag:(TSGGeotag*)geotag;
- (void)clearRegister;
- (void)applyRegister:(NSArray<TSGGeotag*>*)geotags;

@end

@interface TSGTriggerLogic : NSObject
{
    NSString* nowPlayingTitle;
}

+ (void)checkGeotagTriggers:(NSArray<TSGGeotag*>*)geotags
                    onTrack:(int)trackID
                  withRoute:(int)routeID
                 atLocation:(CLLocation*)location
                   callback:(GeotagCallback)callback;
+ (void)triggerTag:(TSGGeotag*)geotag forRoute:(int)routeId callback:(GeotagCallback)callback;

+ (void)setLastTriggered:(TSGGeotag*)geotag;
+ (TSGGeotag*)lastTriggered;
+ (void)clearLastTriggered;

+ (void)setStatus:(BOOL)status;



@end

NS_ASSUME_NONNULL_END

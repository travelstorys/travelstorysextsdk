//
//  TSGAnalyticsManager.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 10/18/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGShorthand.h>


NS_ASSUME_NONNULL_BEGIN

@protocol TSGAnalyticsManagerDelegate <NSObject>

- (void)sendScreen:(NSString *)screenName;
- (void)sendEvent:(NSString *)eventName;
- (void)report:(NSString*)category withAction:(NSString *)action withLabel:(NSString *)label withValue:(double)value;
- (void)reportEvent:(NSString*)eventName withCustom:(NSDictionary*)custom;

- (UIView*)getGADBannerViewWithUnitID:(NSString*)unitID forViewController:(UIViewController*)controller;
- (void)GADReady:(UIView*)gad;

@optional
- (void)report:(NSString*)category withAction:(NSString *)action withLabel:(NSString *)label withValue:(double)value withCustom:(NSDictionary*)custom;

@end

@interface TSGAnalyticsManager : NSObject

+ (void)sendScreen:(NSString*)screenName;
+ (void)sendEvent:(NSString*)eventName;
+ (void)report:(NSString*)category
    withAction:(NSString *)action
     withLabel:(NSString *)label
     withValue:(double)value;

+ (void)setAppName:(NSString*)appName;
+ (void)reportEventWithCategory:(NSString*)category
                     withAction:(NSString*)action
                      withLabel:(NSString*)label
                      withValue:(double)value
                      dataPoint:(NSString*)data
                     dataPoint2:(NSString*)data2;
+ (void)reportEventWithEvent:(NSString*)eventName
                  withCustom:(NSDictionary*)custom;

@end

NS_ASSUME_NONNULL_END

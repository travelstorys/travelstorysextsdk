//
//  TSGTrackSelectListItem.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 1/13/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGRoute.h>
#import <TravelStorysExtSDK/TSGTrack.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGTrackSelectListItem : UITableViewCell

@property IBOutlet UIView* container;
@property IBOutlet UIImageView* icon;
@property IBOutlet UILabel* titleLabel;
@property IBOutlet UILabel* subtitleLabel;

@property (nonatomic) TSGTrack* track;

@end

NS_ASSUME_NONNULL_END

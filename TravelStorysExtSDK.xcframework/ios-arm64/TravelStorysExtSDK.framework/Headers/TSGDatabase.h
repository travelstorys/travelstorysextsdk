//
//  TSGDatabase.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/25/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <TravelStorysExtSDK/TSGCRO.h>
#import <TravelStorysExtSDK/TSGDRO.h>
#import <TravelstorysExtSDK/TSGERO.h>
#import <TravelStorysExtSDK/TSGGeotag.h>
#import <TravelStorysExtSDK/TSGGeotagImage.h>
#import <TravelStorysExtSDK/TSGOrg.h>
#import <TravelStorysExtSDK/TSGRoute.h>
#import <TravelStorysExtSDK/TSGMapOverlay.h>
#import <TravelStorysExtSDK/TSGECoupon.h>

#import <TravelStorysExtSDK/TSGNSArray+JSON.h>
#import <TravelStorysExtSDK/TSGNSDictionary+JSON.h>

#define AssetTypeAudioIntro     @"audio_intro"
#define AssetTypeAudioMusic     @"audio_music"
#define AssetTypeAudioGeotag    @"audio_geotag"
#define AssetTypeAudioTieredIntro @"tiered_audio_intro"
#define AssetTypeKML            @"kml"
#define AssetTypeLogo           @"logo"
#define AssetTypeText           @"text"
#define AssetTypeXML            @"xml"
#define AssetTypeDiscoveryImage @"discovery_image"
#define AssetTypeCityImage      @"city_image"
#define AssetTypeGeotagImage    @"geotag_image"
#define AssetTypeTrackImage     @"track_image"
#define AssetTypePreviewImage   @"preview_image"
#define AssetTypeConnectImage   @"connect_image"
#define AssetTypeConnectLogo    @"connect_logo"
#define AssetTypeAdImage        @"ad_image"
#define AssetTypeReferrerImage  @"referrer_image"
#define AssetTypeFunFact        @"fun_fact"
#define AssetTypeCustomPin      @"custom_pin"
#define AssetTypeFloorPlan      @"floorplan"
#define AssetTypeGeotagTimecode @"geotag_timecode"
typedef NSString* TSGAssetType;

NS_ASSUME_NONNULL_BEGIN

@interface TSGDatabase : NSObject

@property (nonatomic) NSArray<TSGCRO*>* cros;
@property (nonatomic) NSArray<TSGDRO*>* dros;
@property (nonatomic) NSArray<TSGERO*>* eros;

@property (atomic) NSMutableArray* requiredAssets;

+ (TSGDatabase*)shared;

- (void)loadLocal;

- (void)loadCrosFromAPI:(NSArray<NSDictionary*>*)data callback:(void(^)(NSArray<TSGCRO*>* needsUpdate))callback;
- (TSGERO*)loadEROForRouteWithKey:(NSString*)routeKey;
- (TSGDRO*)droForRouteWithKey:(NSString*)routeKey;
- (TSGRoute*)loadRouteForRouteWithKey:(NSString*)routeKey;
- (void)preloadEROs;

- (void)updateDros:(nullable NSArray<TSGDRO*>*)dros;

- (void)saveRoute:(TSGRoute*)route;
- (void)saveTrack:(TSGTrack*)track inRoute:(NSString *)routeKey;
- (TSGTrack*)loadTrackWithId:(int)trackId inRoute:(NSString*)routeKey;
- (void)saveGeotag:(TSGGeotag*)geotag inRoute:(NSString* )routeKey;
- (TSGGeotag*)loadGeotagWithKey:(NSString*)geotagKey inRoute:(NSString*)routeKey;
- (void)saveOrg:(TSGOrg*)org;
- (TSGOrg*)loadOrgWithId:(int)orgId;
- (void)saveMapOverlay:(TSGMapOverlay*)mapOverlay inRoute:(NSString*)route;
- (TSGMapOverlay*)loadMapOverlayWithId:(int)overlayId inRoute:(NSString*)routeKey;
- (void)saveECoupon:(TSGECoupon*)eCoupon inRoute:(NSString*)route;
- (TSGECoupon*)loadECouponWithId:(int)couponId inRoute:(NSString*)routeKey;

- (void)addAssetsToRequiredDatabase:(NSArray<NSString*>*)assets;
- (void)trimRequiredAssets;
- (NSArray<NSString*>*)getRequiredAssets;

- (NSString*)getAssetURL:(TSGAssetType)type inRoute:(TSGRoute*)route;
- (NSString*)getAssetWithMode:(NSString*)mode withId:(int)assetID inRoute:(TSGRoute*)route;
- (NSArray*)getAssetsWithType:(TSGAssetType)type inRoute:(TSGRoute*)route;

- (BOOL)isRouteFavorited:(NSString*)routeKey;
- (void)setRouteFavorite:(BOOL)favorite forRoute:(NSString*)routeKey;
- (NSArray<NSString*>*)getFavoriteRoutes;

@end

NS_ASSUME_NONNULL_END

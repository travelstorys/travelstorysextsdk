//
//  TSGOrg.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/14/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <TravelStorysExtSDK/TSGJsonify.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGOrg : NSObject <TSGJsonable>

@property int orgId;
@property BOOL live;
@property NSString* name;
@property NSString* address;
@property NSString* address2;
@property NSString* city;
@property NSString* state;
@property NSString* zip;
@property NSString* phone;
@property NSString* email;
@property NSString* website;
@property NSString* donateLink;
@property NSString* donateTextNumber;
@property NSString* donateTextPhrase;
@property NSString* donateMessage;
@property NSString* donatePrompt;
@property NSString* donateRecipient;
@property NSString* donateTitle;
@property NSString* learnText;
@property NSString* logoURL;
@property NSString* facebook;
@property NSString* twitter;
@property NSString* youtube;
@property NSString* instagram;
@property NSString* pinterest;

@end

NS_ASSUME_NONNULL_END

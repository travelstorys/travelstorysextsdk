//
//  TSGAcknowledgementsViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 1/26/21.
//  Copyright © 2021 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <TravelStorysExtSDK/TSGRoute.h>

#import <TravelStorysExtSDK/ITSGViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGAcknowledgementsViewController : UIViewController <
    ITSGViewController,

    UITableViewDelegate,
    UITableViewDataSource
>

@property IBOutlet UITableView* tableView;
@property IBOutlet UIButton* closeButton;

@property IBOutlet UIView* swipeInterceptView;

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;
@property (readonly, nonatomic) TSGRoute* route;


- (void)loadRoute:(TSGRoute*)route;

@end

NS_ASSUME_NONNULL_END

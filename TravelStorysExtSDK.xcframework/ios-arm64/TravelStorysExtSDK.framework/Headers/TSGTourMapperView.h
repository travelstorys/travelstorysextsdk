//
//  TSGTourMapperView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/28/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <MapKit/MapKit.h>

#import <TravelStorysExtSDK/TSGDatabase.h>
#import <TravelStorysExtSDK/TSGDataManager.h>
#import <TravelStorysExtSDK/TSGDownloadManager.h>
#import <TravelStorysExtSDK/TSGGeotag.h>
#import <TravelStorysExtSDK/TSGGeotagAnnotation.h>
#import <TravelStorysExtSDK/TSGDeepLinkHandler.h>
#import <TravelStorysExtSDK/TSGRoute.h>
#import <TravelStorysExtSDK/TSGTrack.h>

#import <TravelStorysExtSDK/TSGTriggerLogic.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum TSGMapMode : UInt8
{
    MapModeOutdoor,
    MapModeIndoor,
    MapModeHybrid
} TSGMapMode;

@protocol TSGTourMapperDelegate <NSObject>

- (void)annotationClicked:(TSGGeotagAnnotation*)annotation;
- (void)zoomChanged:(float)zoomScale;

@end

@interface TSGTourMapperView : MKMapView <MKMapViewDelegate>

- (void)loadRoute:(TSGRoute*)route withTrack:(TSGTrack*)track;
- (void)loadRoute:(TSGRoute*)route withTrack:(TSGTrack*)track withMode:(TSGMapMode)viewMode;
- (void)refreshTriggerOverlays;

- (void)showIndoorMap:(TSGRoute*)route withTrack:(TSGTrack*)track floor:(int)floor shouldMove:(BOOL)move indoorOnly:(BOOL)indoorOnly;
- (void)showIndoorMap:(TSGRoute*)route withTrack:(TSGTrack*)track floor:(int)floor shouldMove:(BOOL)move;

- (void)showTag:(TSGGeotag*)geotag;

- (NSArray<TSGGeotag*>*)getMappedGeotags;

- (NSDictionary*)getFloorData;

- (void)prepareForBackground;
- (void)prepareForForeground;

@property (nonatomic) id<TSGTourMapperDelegate> mapperDelegate;

@property (nonatomic, readonly) TSGRoute* route;
@property (nonatomic, readonly) TSGTrack* track;

@property (nonatomic) BOOL hideButtons;
@property (nonatomic) TSGMapMode currentMapMode;

@property (nonatomic) UIViewController* activeMapViewController;

@end

@interface IndoorBlankRenderer : MKOverlayRenderer

@end

NS_ASSUME_NONNULL_END
